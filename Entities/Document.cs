﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Entities
{
    public class Document
    {
        public int DocumentID
        {
            get;set;
        }
        public string stringDocumentID
        {
            get;set;
        }
        public string Type
        {
            get;set;
        }
        public string Name
        {
            get;set;
        }
        public string DocUrl
        {
            get;set;
        }
        public string DocName
        {
            get;set;
        }
        public string Tags
        {
            get;set;
        }
    }
}
