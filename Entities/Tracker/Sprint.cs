﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Entities.Tracker
{
    public class Sprint
    {
        public int SprintID
        {
            get;set;
        }
        public string Name
        {
            get;set;
        }
        public DateTime StarDate
        {
            get;set;
        }
        public DateTime EndDate
        {
            get;set;
        }
        public string DateRange
        {
            get;set;
        }
        public int StoryPoints_Committed
        {
            get;set;
        }
        public int StoryPoints_Delivered
        {
            get;set;
        }
        public int Bugs_Raised
        {
            get;set;
        }
        public int Patches_Count
        {
            get;set;
        }
        public string Comments
        {
            get;set;
        }
        public string ReleaseName
        {
            get;set;
        }
        public string ReleaseDate
        {
            get; set;
        }
    }
}
