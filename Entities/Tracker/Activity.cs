﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Entities.Tracker
{
    public class Activity
    {
        public int ActivityID
        {
            get; set;
        }
        public string StringActivityID
        {
            get; set;
        }
        public string Name
        {
            get; set;
        }
        public string Detail
        {
            get; set;
        }
        public string Tags
        {
            get; set;
        }
        public DateTime CreatedOn
        {
            get;set;
        }
        public string DocUrl
        {
            get;set;
        }
        public string DocName
        {
            get;set;
        }
    }
}
