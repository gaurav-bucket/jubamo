﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Entities.Jubamo
{
    public class IMDB
    {
        public string ImdbID
        {
            get; set;
        }
        public IList<IMDB> Search
        {
            get; set;
        }
        public string TotalResults
        {
            get; set;
        }
        public string Response
        {
            get; set;
        }
        public string Title
        {
            get; set;
        }
        public string Type
        {
            get; set;
        }
        public string Year
        {
            get; set;
        }
        public string Rating
        {
            get; set;
        }
        public string Plot
        {
            get; set;
        }

        public string Poster
        {
            get; set;
        }
        public string ImdbRating
        {
            get; set;
        }
        public string Genre
        {
            get; set;
        }
        public string Language
        {
            get; set;

        }
    }
}
