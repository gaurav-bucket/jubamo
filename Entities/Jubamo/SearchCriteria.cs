﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Entities.Jubamo
{
    public class SearchCriteria
    {
        public string SearchQuery
        {
            get;set;
        }
        public string SearchMode
        {
            get;set;
        }
        public IList<string> SearchFields
        {
            get;set;
        }
        public int Top
        {
            get;set;
        }
        public int Skip
        {
            get;set;
        }
        public string Filter
        {
            get;set;
        }
        public IList<string> OrderBy
        {
            get;set;
        }
        public IList<string> Facets
        {
            get;set;
        }
    }
}
