﻿using System;

namespace Entities
{
    public class Content
    {
        public int ContentID
        {
            get; set;
        }
        public string StringContentID
        {
            get; set;
        }
        public int Sequence
        {
            get; set;
        }

        public string Type
        {
            get; set;
        }
        public string Name
        {
            get; set;
        }
      
        public string Year
        {
            get; set;
        }
        public string Platform
        {
            get; set;
        }
        public string Language
        {
            get; set;
        }
        public string Genre
        {
            get; set;
        }
        public string Rating
        {
            get; set;
        }
        public float ImdbRating
        {
            get; set;
        }
        public string Plot
        {
            get; set;
        }
        public string Comment
        {
            get; set;
        }
        public string Watchlink
        {
            get; set;
        }
        public string ImageName
        {
            get; set;
        }
        public string ImageSource
        {
            get;set;
        }
        public string ImageUrl
        {
            get;set;
        }
        public string Tags
        {
            get;set;
        }
        public string IsAvailableSection
        {
            get; set;
        }
        public string IsRecentlyReleasedSection
        {
            get; set;
        }
        public string IsTopSection
        {
            get; set;
        }
        public int Status
        {
            get;set;
        }
        public string ImdbID
        {
            get; set;
        }
    }
}
