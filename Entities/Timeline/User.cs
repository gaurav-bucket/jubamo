﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Entities.Timeline
{
    public class User
    {
        public int UserID
        {
            get;set;
        }
        public string Name
        {
            get;set;
        }
        public string Password
        {
            get;set;
        }
    }
}
