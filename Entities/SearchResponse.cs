﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Entities
{
    public class SearchResponse
    {
        public SearchResponse()
        {

        }
        public string StringContentID
        {
            get; set;
        }

        public string Type
        {
            get; set;
        }
        public string Name
        {
            get; set;
        }
        public string Year
        {
            get; set;
        }
        public string Platform
        {
            get; set;
        }
        public string Language
        {
            get; set;
        }
        public string Genre
        {
            get; set;
        }
        public string Rating
        {
            get; set;
        }
        public string Plot
        {
            get; set;
        }
        public string Comment
        {
            get; set;
        }
        public string Watchlink
        {
            get; set;
        }
        public string ImageUrl
        {
            get; set;
        }

        public string Tags
        {
            get; set;
        }
    }
}
