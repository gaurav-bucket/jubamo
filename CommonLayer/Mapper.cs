﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Entities;
using Microsoft.Azure.Search.Models;

namespace CommonLayer
{
    public class Mapper
    {
        public IList<Entities.Document> MapToDocument(DocumentSearchResult<Entities.Document> searchResult)
        {
            IList<Entities.Document> documents = new List<Entities.Document>();
            Entities.Document document = null;
            foreach (SearchResult<Entities.Document> result in searchResult.Results)
            {
                document = new Entities.Document();
                document.DocumentID = Convert.ToInt32(result.Document.stringDocumentID);
                document.Type = result.Document.Type;
                document.Name = result.Document.Name;
                document.DocUrl = result.Document.DocUrl;
                document.DocName = Path.GetFileName(result.Document.DocUrl);
                documents.Add(document);
            }
            return documents;
        }
        public IList<Entities.Tracker.Activity> MapToActvity(DocumentSearchResult<Entities.Tracker.Activity> searchResult)
        {
            IList<Entities.Tracker.Activity> activites = new List<Entities.Tracker.Activity>();
            Entities.Tracker.Activity activity = null;
            foreach (SearchResult<Entities.Tracker.Activity> result in searchResult.Results)
            {
                activity = new Entities.Tracker.Activity();
                activity.ActivityID = Convert.ToInt32(result.Document.StringActivityID);
                activity.Name = result.Document.Name;
                activity.Detail = result.Document.Detail;
                activity.Tags = result.Document.Tags;
                activity.CreatedOn = result.Document.CreatedOn;
                activity.DocUrl = result.Document.DocUrl;
                activity.DocName = Path.GetFileName(result.Document.DocUrl);
                activites.Add(activity);
            }
            return activites;
        }
    }
}
