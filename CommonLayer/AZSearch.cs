﻿using Microsoft.Azure.Search;
using Microsoft.Azure.Search.Models;
using System;
using System.Collections;
using System.Collections.Generic;

namespace CommonLayer
{
    public class AZSearch<T>
    {
        string azureSearchName = string.Empty;
        string azureSearchKey = string.Empty;
        string indexName = string.Empty;
        string searchText = string.Empty;
        string filterText = string.Empty;
        public AZSearch(string azureSearchName,string azureSearchKey,string indexName,string searchText,string filterText = "")
        {
            this.azureSearchName = azureSearchName;
            this.azureSearchKey = azureSearchKey;
            this.indexName = indexName;
            this.searchText = searchText;
            this.filterText = filterText;
        }
        public DocumentSearchResult<T> GetResults()
        {
            ISearchServiceClient azureSearchService = new SearchServiceClient(azureSearchName, new SearchCredentials(azureSearchKey));
            ISearchIndexClient indexClient = azureSearchService.Indexes.GetClient(indexName);
            //var sp = new SearchParameters();
            //DocumentSearchResult<T> response = indexClient.Documents.Search<T>(searchText, sp);
            var sp = new SearchParameters()
            {
                Filter = filterText
                //Select = new[] { "StringActivityID", "Name","Detail","Tags","CreatedOn" }
            };

            DocumentSearchResult<T> response = indexClient.Documents.Search<T>(searchText, sp);
            return response;
            //IList<T> contents = new List<T>();
            //T content = default(T);
            //foreach (SearchResult<T> result in response.Results)
            //{
            //    //content = new T();
            //    content.ContentID = Convert.ToInt32(result.Document.StringContentID);
            //    content.Type = result.Document.Type;
            //    content.Name = result.Document.Name;
            //    content.Platform = result.Document.Platform;
            //    content.Year = result.Document.Year;
            //    content.Language = result.Document.Language;
            //    content.Genre = result.Document.Genre;
            //    content.Rating = result.Document.Rating;
            //    content.Watchlink = result.Document.Watchlink;
            //    content.ImageUrl = result.Document.ImageUrl;
            //    content.Plot = result.Document.Plot;
            //    content.Tags = result.Document.Tags;
            //    contents.Add(content);
            //}
            //return contents;
            //if (response.Facets != null)
            //{
            //    foreach (var facet in response.Facets)
            //    {
            //        Console.WriteLine("\n Facet Name: " + facet.Key);
            //        foreach (var value in facet.Value)
            //        {
            //            Console.WriteLine("Value :" + value.Value + " - Count: " + value.Count);
            //        }
            //    }
            //}
        }
    }
}
