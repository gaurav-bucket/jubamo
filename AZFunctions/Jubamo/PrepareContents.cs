using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System.Data;
using System.Data.SqlClient;

namespace AZFunctions.Jubamo
{
    public static class PrepareContents
    {
        [FunctionName("PrepareContents")]
        public static async Task<IActionResult> Run(
            [HttpTrigger(AuthorizationLevel.Function, "get", "post", Route = null)] HttpRequest req,
            ILogger log)
        {
            int result = 0;
            result = Insert();

            return new OkObjectResult(result.ToString());
        }
        public static int Insert()
        {
            var conString = Environment.GetEnvironmentVariable("ConnectionString");
            var con = new SqlConnection(conString);

            con.Open();
            IDbCommand com = new SqlCommand();
            com.Connection = con;
            try
            {
                com = new SqlCommand("spInsertContents", con);
                com.CommandType = CommandType.StoredProcedure;
                int result = com.ExecuteNonQuery();
                return result;
            }
            catch (Exception ex)
            {
                return -2;
            }

        }
    }
}
