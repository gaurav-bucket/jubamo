﻿using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Microsoft.Azure.Search;
using Microsoft.Azure.Search.Models;
using System.Collections.Generic;
using Entities;
using System.Data.SqlClient;
using System.Data;
using Microsoft.Azure;
using Microsoft.Azure.Storage;
using System.Configuration;

namespace AZFunctions.Jubamo
{
    public class ContentIndexing
    {
        [FunctionName("Jubamo-Indexing")]
        public static IActionResult Run(
            [HttpTrigger(AuthorizationLevel.Function, "get", "post", Route = null)] HttpRequest req,
            ILogger log)
        {
            string responseMessage = string.Empty;
            IList<ContentIndexModel> contentIndexModels = Map(GetContents());
            if (contentIndexModels == null || contentIndexModels.Count == 0)
            {
                responseMessage = "no data to index";
                return new OkObjectResult(responseMessage);
            }
            responseMessage = DoIndexing(contentIndexModels);
            if (responseMessage == "success")
            {
                responseMessage = UpdateIndexedItems(contentIndexModels);
            }
            return new OkObjectResult(responseMessage);
        }
        public static IList<Content> GetContents()
        {
            var conString = Environment.GetEnvironmentVariable("ConnectionString");
            var con = new SqlConnection(conString);
            con.Open();
            IList<Content> contents = null;
            IDbCommand com = new SqlCommand();
            com.Connection = con;
            com.CommandText = "spGetDataToIndex";
            com.CommandType = CommandType.StoredProcedure;
            SqlDataReader reader = (SqlDataReader)com.ExecuteReader();
            if (reader.HasRows)
            {
                contents = new List<Content>();
                while (reader.Read())
                {
                    Content content = new Content();
                    content.ContentID = Convert.ToInt32(reader["ContentID"].ToString());
                    //content.ContentID = reader["ContentID"].ToString();
                    content.StringContentID = reader["ContentID"].ToString();
                    content.Type = reader["Type"].ToString();
                    content.Name = reader["Name"].ToString();
                    content.Year = reader["Year"].ToString();
                    content.Platform = reader["Platform"].ToString();
                    content.Language = reader["Language"].ToString();
                    content.Genre = reader["Genre"].ToString();
                    content.Rating = reader["Rating"].ToString();
                    content.ImdbRating = float.Parse(reader["ImdbRating"].ToString());
                    content.Comment = reader["Comment"].ToString();
                    content.Watchlink = reader["WatchLink"].ToString();
                    content.ImageName = reader["ImageName"].ToString();
                    content.ImageSource = reader["ImageSource"].ToString();
                    content.Plot = reader["Plot"].ToString();
                    content.Sequence = Convert.ToInt32(reader["Sequence"].ToString());
                    content.Tags = reader["tags"].ToString();
                    content.IsAvailableSection = reader["IsAvailableSection"].ToString();
                    content.IsRecentlyReleasedSection = reader["IsRecentlyReleasedSection"].ToString();
                    content.IsTopSection = reader["IsTopSection"].ToString();
                    contents.Add(content);
                }
            }
            reader.Close();
            return contents;
        }
        public static IList<ContentIndexModel> Map(IList<Content> contents)
        {
            IList<ContentIndexModel> contentIndexModels = new List<ContentIndexModel>();
            ContentIndexModel contentIndexModel = null;
            if (contents == null || contents.Count == 0)
            {
                return contentIndexModels;
            }
            foreach (var item in contents)
            {
                contentIndexModel = new ContentIndexModel();
                contentIndexModel.StringContentID = item.ContentID.ToString();
                contentIndexModel.Type = item.Type;
                contentIndexModel.Sequence = item.Sequence;
                contentIndexModel.Name = item.Name;
                contentIndexModel.Year = item.Year;
                contentIndexModel.Platform = item.Platform;
                contentIndexModel.Language = item.Language;
                contentIndexModel.Genre = item.Genre;
                contentIndexModel.Rating = item.Rating;
                contentIndexModel.ImdbRating = item.ImdbRating;
                contentIndexModel.Comment = item.Comment;
                contentIndexModel.Watchlink = item.Watchlink;
                contentIndexModel.Plot = item.Plot;
                contentIndexModel.ImageName = item.ImageName;
                contentIndexModel.ImageSource = item.ImageSource;
                contentIndexModel.Tags = item.Tags;
                contentIndexModel.IsAvailableSection = item.IsAvailableSection;
                contentIndexModel.IsRecentlyReleasedSection = item.IsRecentlyReleasedSection;
                contentIndexModel.IsTopSection = item.IsTopSection;
                contentIndexModels.Add(contentIndexModel);
            }
            return contentIndexModels;

        }
        public static string DoIndexing(IList<ContentIndexModel> contentIndexModels)
        {
            if (contentIndexModels == null || contentIndexModels.Count == 0)
            {
                return "no data to index";
            }

            var azureSearchName = Environment.GetEnvironmentVariable("AzureSearchName"); //"azuresearchforjubamo";
            var azureSearchKey = Environment.GetEnvironmentVariable("AzureSearchKey"); //"FC2012375F143A8E695F273FD885F61E";
            string indexName = Environment.GetEnvironmentVariable("IndexName"); //"jubamo-development";
            ISearchServiceClient azureSearchService = new SearchServiceClient(azureSearchName, new SearchCredentials(azureSearchKey));
            ISearchIndexClient indexClient = azureSearchService.Indexes.GetClient(indexName);
            //if (azureSearchService.Indexes.Exists(indexName))
            //{
            //    azureSearchService.Indexes.Delete(indexName);
            //}
            Index indexModel = new Index()
            {
                Name = indexName,
                Fields = new[]
                {
                    new Field("StringContentID",DataType.String) { IsKey = true, IsRetrievable = true, IsFacetable = false },
                    new Field("Type", DataType.String) {IsRetrievable = true, IsSearchable = false, IsFacetable = false },
                    new Field("Name", DataType.String) {IsRetrievable = true, IsSearchable = true, IsFilterable = true,IsFacetable = false,IsSortable = true, },
                    new Field("Sequence", DataType.Int32) {IsRetrievable = true, IsSearchable = false, IsFilterable = false,IsFacetable = false,IsSortable = true, },
                    new Field("Year", DataType.String) {IsSearchable = false, IsRetrievable = true, IsFilterable = true, IsFacetable = false },
                    new Field("Platform", DataType.String) { IsSearchable=false, IsFilterable = true, IsRetrievable = true, IsSortable = true, IsFacetable = false },
                    new Field("Language", DataType.String) { IsSearchable=false, IsFacetable = true, IsFilterable= true, IsRetrievable = true },
                    new Field("Genre", DataType.String) { IsSearchable=false, IsRetrievable = true, IsFacetable = false },
                    new Field("Rating", DataType.String) {IsRetrievable = true, IsSearchable = false, IsFacetable = false },
                    new Field("ImdbRating", DataType.Double) {IsRetrievable = true, IsSearchable = false, IsFilterable = true,IsFacetable = false,IsSortable = true, },
                    new Field("Comment", DataType.String) {IsSearchable = false, IsRetrievable = true, IsFilterable = true, IsFacetable = false },
                    new Field("Watchlink", DataType.String) { IsSearchable=false, IsFilterable = true, IsRetrievable = true, IsSortable = true, IsFacetable = false },
                    new Field("Plot", DataType.String) { IsSearchable=false, IsFilterable = true, IsRetrievable = true, IsSortable = true, IsFacetable = false },
                    new Field("ImageName", DataType.String) { IsSearchable=false, IsFilterable = true, IsRetrievable = true, IsSortable = true, IsFacetable = false },
                    new Field("ImageSource", DataType.String) { IsSearchable=false, IsFilterable = true, IsRetrievable = true, IsSortable = true, IsFacetable = false },
                    new Field("Tags", DataType.String) { IsSearchable = true,IsFilterable = true, IsRetrievable = true, IsSortable = true, IsFacetable = false },
                    new Field("IsTopSection", DataType.String) { IsSearchable = true,IsFilterable = true, IsRetrievable = true, IsSortable = true, IsFacetable = false },
                    new Field("IsAvailableSection", DataType.String) { IsSearchable = true,IsFilterable = true, IsRetrievable = true, IsSortable = true, IsFacetable = false },
                    new Field("IsRecentlyReleasedSection", DataType.String) { IsSearchable = true,IsFilterable = true, IsRetrievable = true, IsSortable = true, IsFacetable = false },
                }
            };


            // Create the Index in AzureSearch
            var resultIndex = azureSearchService.Indexes.CreateOrUpdate(indexModel);
            //var listBooks = new BookModel().GetBooks();
            indexClient.Documents.Index(IndexBatch.MergeOrUpload<ContentIndexModel>(contentIndexModels));
            return "success";
        }
        public static string UpdateIndexedItems(IList<ContentIndexModel> contentIndexModels)
        {
            var conString = Environment.GetEnvironmentVariable("ConnectionString");
            var con = new SqlConnection(conString);

            con.Open();
            IDbCommand com = new SqlCommand();
            com.Connection = con;
            DataTable ContentIDTable = new DataTable();
            ContentIDTable.Columns.Add("ContentID", typeof(Int32));
            DataRow row = null;
            try
            {
                foreach (var item in contentIndexModels)
                {
                    row = ContentIDTable.NewRow();
                    row["ContentID"] = Convert.ToInt32(item.StringContentID);
                    ContentIDTable.Rows.Add(row);
                }

                com = new SqlCommand("spUpdateIndexValues", con);
                com.CommandType = CommandType.StoredProcedure;
                SqlParameter param = new SqlParameter("@ContentIDs", ContentIDTable);
                param.SqlDbType = SqlDbType.Structured;
                param.TypeName = "dbo.ContentIDTVP";
                com.Parameters.Add(param);
                com.ExecuteNonQuery();
                return "Data Updated Successfully";
            }
            catch (Exception ex)
            {
                return "Some error occured in data updation " + ex.Message + "stack trace " + ex.StackTrace
                    + "Inner Exception " + ex.InnerException;
            }

        }
    }
}
