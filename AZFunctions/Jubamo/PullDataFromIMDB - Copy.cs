using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System.Net.Http;
using System.Data.SqlClient;
using System.Data;
using System.Collections.Generic;
using Entities;

namespace AZFunctions.Jubamo
{
    public static class PullDataFromIMDB_Backup
    {
    //    [FunctionName("PullDataFromIMDB_Backup")]
    //    public static async Task<IActionResult> Run(
    //        [HttpTrigger(AuthorizationLevel.Function, "get", "post", Route = null)] HttpRequest req,
    //        ILogger log)
    //    {
    //        IList<Content> contents = GetDataToPullFromImdb();
    //        IList<IMDB> lstImdb = new List<IMDB>();
    //        var client = new HttpClient();
    //        HttpRequestMessage httpRequestMessage = null;
    //        foreach(var item in contents)
    //        {
    //            httpRequestMessage = new HttpRequestMessage
    //            {
    //                Method = HttpMethod.Get,
    //                RequestUri = new Uri("https://imdb-internet-movie-database-unofficial.p.rapidapi.com/film/" + item.Name),
    //                Headers =
    //                    {
    //                        { "x-rapidapi-key", "18a875f440msh70f4796d50bfb5bp16167ejsn00a47c841bc4" },
    //                        { "x-rapidapi-host", "imdb-internet-movie-database-unofficial.p.rapidapi.com" },
    //                    },
    //            };
    //            using (var response = await client.SendAsync(httpRequestMessage))
    //            {
    //                response.EnsureSuccessStatusCode();
    //                //var body = await response.Content.ReadAsStringAsync();
    //                var body = await response.Content.ReadAsStringAsync();
    //                IMDB imdb = JsonConvert.DeserializeObject<IMDB>(body);
    //                lstImdb.Add(imdb);
    //            }
    //        }

    //        contents = Map(lstImdb, contents);
    //        string responseMessage = Update(contents);
    //        return new OkObjectResult(responseMessage);
    //    }

    //    public static IList<Content> GetDataToPullFromImdb ()
    //    {
    //        var conString = Environment.GetEnvironmentVariable("ConnectionString");
    //        var con = new SqlConnection(conString);
    //        con.Open();
    //        IList<Content> contents = null;
    //        IDbCommand com = new SqlCommand();
    //        com.Connection = con;
    //        com.CommandText = "spGetDataToPullFromImdb";
    //        com.CommandType = CommandType.StoredProcedure;
    //        SqlDataReader reader = (SqlDataReader)com.ExecuteReader();
    //        if (reader.HasRows)
    //        {
    //            contents = new List<Content>();
    //            while (reader.Read())
    //            {
    //                Content content = new Content();
    //                content.ContentID = Convert.ToInt32(reader["ContentID"].ToString());
    //                content.Name = reader["Name"].ToString();
    //                content.Genre = reader["Genre"].ToString();
    //                contents.Add(content);
    //            }
    //        }
    //        reader.Close();
    //        return contents;
    //    }
    //    public static IList<Content> Map(IList<IMDB> source,IList<Content> destination)
    //    {
    //        //IList<Content> contents = new List<Content>();
    //        //Content content = null;
    //        //if (lstImdb == null || lstImdb.Count == 0)
    //        //{
    //        //    return contents;
    //        //}
    //        foreach (var sourceItem in source)
    //        {
    //            foreach(var destinationItem in destination)
    //            {
    //                if(sourceItem.Title.Trim() == destinationItem.Name)
    //                {
    //                    destinationItem.Year = sourceItem.Year;
    //                    destinationItem.Rating = sourceItem.Rating;
    //                    destinationItem.Plot = sourceItem.Plot;
    //                    destinationItem.ImageUrl = sourceItem.Poster;
    //                    if(sourceItem.Year == "" || sourceItem.Rating == "" || sourceItem.Plot == "")
    //                    {
    //                        destinationItem.Status = 1;
    //                    }
    //                    else
    //                    {
    //                        destinationItem.Status = 2;
    //                    }
    //                }

    //            }
    //        }
    //        return destination;

    //    }
    //    public static string Update(IList<Content> contents)
    //    {
    //        var conString = Environment.GetEnvironmentVariable("ConnectionString");
    //        var con = new SqlConnection(conString);

    //        con.Open();
    //        IDbCommand com = new SqlCommand();
    //        com.Connection = con;
    //        try
    //        {
    //            com = new SqlCommand("spUpdateData", con);
    //            com.CommandType = CommandType.StoredProcedure;
    //            SqlParameter param = null;
    //            foreach (var item in contents)
    //            {
    //                param = new SqlParameter("@ContentID", item.ContentID);
    //                param.SqlDbType = SqlDbType.Int;
    //                com.Parameters.Add(param);
    //                param = new SqlParameter("@Year", item.Year);
    //                param.SqlDbType = SqlDbType.VarChar;
    //                com.Parameters.Add(param);
    //                param = new SqlParameter("@Rating", item.Rating);
    //                param.SqlDbType = SqlDbType.VarChar;
    //                com.Parameters.Add(param);
    //                param = new SqlParameter("@ImageUrl", item.ImageUrl);
    //                param.SqlDbType = SqlDbType.VarChar;
    //                com.Parameters.Add(param);
    //                param = new SqlParameter("@Status", item.Status);
    //                param.SqlDbType = SqlDbType.Int;
    //                com.Parameters.Add(param);
    //                com.ExecuteNonQuery();
    //            }
    //            return "Data Updated Successfully";
    //        }
    //        catch (Exception ex)
    //        {
    //            return "Some error occured in data updation " + ex.Message + "stack trace " + ex.StackTrace
    //                + "Inner Exception " + ex.InnerException;
    //        }

    //    }
    }
}
