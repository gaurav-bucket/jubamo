using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System.Net.Http;
using System.Data.SqlClient;
using System.Data;
using System.Collections.Generic;
using Entities;
using Entities.Jubamo;

namespace AZFunctions.Jubamo
{
    public static class PullDataFromIMDB
    {
        [FunctionName("PullDataFromIMDB")]
        public static async Task<IActionResult> Run(
            [HttpTrigger(AuthorizationLevel.Function, "get", "post", Route = null)] HttpRequest req,
            ILogger log)
        {
            IList<Content> contents = GetTitles();
            Content content = null;
            IList<IMDB> lstImdb = new List<IMDB>();
            IMDB imdb = null;
            int result = 0;
            var client = new HttpClient();
            string imdbID = string.Empty;
            HttpRequestMessage httpRequestMessage = null;
            string message = string.Empty;
            try
            {
                if (contents != null && contents.Count > 0)
                {
                    foreach (var item in contents)
                    {
                        httpRequestMessage = new HttpRequestMessage
                        {
                            Method = HttpMethod.Get,
                            RequestUri = new Uri("https://movie-database-imdb-alternative.p.rapidapi.com/?s=" + item.Name + "& page=1&r=json"),
                            Headers =
    {
        { "x-rapidapi-key", "18a875f440msh70f4796d50bfb5bp16167ejsn00a47c841bc4" },
        { "x-rapidapi-host", "movie-database-imdb-alternative.p.rapidapi.com" },
    },
                        };
                        using (var response = await client.SendAsync(httpRequestMessage))
                        {
                            try
                            {
                                response.EnsureSuccessStatusCode();
                                var body = await response.Content.ReadAsStringAsync();
                                imdb = JsonConvert.DeserializeObject<IMDB>(body);
                                if (imdb.Response != "False")
                                {
                                    imdbID = GetImdbID(item.Name, imdb);
                                }
                                imdb = await GetDataFromIMDB(imdbID);
                                if (imdb.Response != "False")
                                {
                                    try
                                    {
                                        content = Map(imdb, item);
                                        result = Insert(content);
                                        if (result > 0)
                                        {
                                            message = "Success";
                                        }
                                        else
                                        {
                                            message = "Error during saving";
                                        }
                                        Update(content.Name, result, message);
                                    }
                                    catch (Exception ex)
                                    {
                                        Update(item.Name, -1, ex.Message);
                                    }

                                }
                                else
                                {
                                    Update(item.Name, -4, "Response is false from IMDB");
                                }

                            }
                            catch (Exception ex)
                            {
                                result = -3;
                            }
                        }
                    }
                    message = "Success";
                }
                else
                {
                    message = "No titles to fetch data";
                }
            }
            catch(Exception ex)
            {
                message = ex.ToString();
            }

            string responseMessage = message;
            return new OkObjectResult(responseMessage);
        }


        public static string GetImdbID(string name,IMDB imdb)
        {
            foreach (var item in imdb.Search)
            {
                if (item.Title == name)
                {
                    return item.ImdbID;
                }
            }
            return string.Empty;
        }
        public static async Task<IMDB> GetDataFromIMDB(string imdbID)
        {
            var client = new HttpClient();
            IMDB imdb = null;
            var request = new HttpRequestMessage
            {
                Method = HttpMethod.Get,
                RequestUri = new Uri("https://movie-database-imdb-alternative.p.rapidapi.com/?i=" + imdbID + "&r=json"),
                Headers =
    {
        { "x-rapidapi-key", "18a875f440msh70f4796d50bfb5bp16167ejsn00a47c841bc4" },
        { "x-rapidapi-host", "movie-database-imdb-alternative.p.rapidapi.com" },
    },
            };
            using (var response = await client.SendAsync(request))
            {
                response.EnsureSuccessStatusCode();
                var body = await response.Content.ReadAsStringAsync();
                imdb = JsonConvert.DeserializeObject<IMDB>(body);
            }
            return imdb;
        }
        public static IList<Content> GetTitles()
        {
            var conString = Environment.GetEnvironmentVariable("ConnectionString");
            var con = new SqlConnection(conString);
            con.Open();
            IList<Content> contents = null;
            IDbCommand com = new SqlCommand();
            com.Connection = con;
            com.CommandText = "spGetDataToPullFromImdb";
            com.CommandType = CommandType.StoredProcedure;
            SqlDataReader reader = (SqlDataReader)com.ExecuteReader();
            if (reader.HasRows)
            {
                contents = new List<Content>();
                while (reader.Read())
                {
                    Content content = new Content();
                    content.Name = reader["Title"].ToString();
                    content.Platform = reader["Platform"].ToString();
                    content.Watchlink = reader["Watchlink"].ToString();
                    contents.Add(content);
                }
            }
            reader.Close();
            return contents;
        }
        public static Content Map(IMDB source,Content destination)
        {
            //IList<Content> contents = new List<Content>();
            //Content content = null;
            //if (lstImdb == null || lstImdb.Count == 0)
            //{
            //    return contents;
            //}
            destination.Year = source.Year;
            destination.Rating = source.ImdbRating;
            destination.ImdbRating = float.Parse(source.ImdbRating);
            destination.Plot = source.Plot;
            destination.ImageName = source.Poster;
            destination.Language = source.Language;
            destination.Type = source.Type;
            destination.Genre = source.Genre;
            return destination;
        }
        public static int Update(string title,int status,string message)
        {
            var conString = Environment.GetEnvironmentVariable("ConnectionString");
            var con = new SqlConnection(conString);

            con.Open();
            IDbCommand com = new SqlCommand();
            com.Connection = con;
            try
            {
                com = new SqlCommand("spUpdateTitlesStatus", con);
                com.CommandType = CommandType.StoredProcedure;
                SqlParameter param = null;

                param = new SqlParameter("@Title",title);
                param.SqlDbType = SqlDbType.VarChar;
                com.Parameters.Add(param);

                param = new SqlParameter("@Status", status);
                param.SqlDbType = SqlDbType.Int;
                com.Parameters.Add(param);

                param = new SqlParameter("@Message", message);
                param.SqlDbType = SqlDbType.VarChar;
                com.Parameters.Add(param);

                int result = com.ExecuteNonQuery();
                return result;
            }
            catch (Exception ex)
            {
                return -1;
            }

        }
        public static int Insert(Content content)
        {
            var conString = Environment.GetEnvironmentVariable("ConnectionString");
            var con = new SqlConnection(conString);

            con.Open();
            IDbCommand com = new SqlCommand();
            com.Connection = con;
            try
            {
                com = new SqlCommand("spInsertSourceContents", con);
                com.CommandType = CommandType.StoredProcedure;
                SqlParameter param = null;

                param = new SqlParameter("@name", content.Name);
                param.SqlDbType = SqlDbType.VarChar;
                com.Parameters.Add(param);

                if(content.Type == "series")
                {
                    content.Type = "TV Series";
                }
                else if(content.Type == "movie")
                {
                    content.Type = "Movie";
                }
                param = new SqlParameter("@type", content.Type);
                param.SqlDbType = SqlDbType.VarChar;
                com.Parameters.Add(param);

                param = new SqlParameter("@platform", content.Platform);
                param.SqlDbType = SqlDbType.VarChar;
                com.Parameters.Add(param);

                param = new SqlParameter("@language", content.Language);
                param.SqlDbType = SqlDbType.VarChar;
                com.Parameters.Add(param);

                param = new SqlParameter("@genre", content.Genre);
                param.SqlDbType = SqlDbType.VarChar;
                com.Parameters.Add(param);

                param = new SqlParameter("@rating", content.Rating);
                param.SqlDbType = SqlDbType.VarChar;
                com.Parameters.Add(param);

                param = new SqlParameter("@imdbrating", content.ImdbRating);
                param.SqlDbType = SqlDbType.Float;
                com.Parameters.Add(param);

                param = new SqlParameter("@watchlink", content.Watchlink);
                param.SqlDbType = SqlDbType.VarChar;
                com.Parameters.Add(param);

                param = new SqlParameter("@imagename", content.ImageName);
                param.SqlDbType = SqlDbType.VarChar;
                com.Parameters.Add(param);

                param = new SqlParameter("@plot", content.Plot);
                param.SqlDbType = SqlDbType.VarChar;
                com.Parameters.Add(param);

                param = new SqlParameter("@year", content.Year);
                param.SqlDbType = SqlDbType.VarChar;
                com.Parameters.Add(param);

                int result = com.ExecuteNonQuery();
                return result;
            }
            catch (Exception ex)
            {
                return -2;
            }

        }
    }
}
