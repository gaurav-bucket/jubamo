using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using DAL;
using Entities;
using System.Configuration;
using Microsoft.IdentityModel.Protocols;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Data;

namespace AZFunctions
{
    public static class Indexing
    {
        //[FunctionName("Indexing")]
        //public static async Task<IActionResult> Run(
        //    [HttpTrigger(AuthorizationLevel.Function, "get", "post", Route = null)] HttpRequest req,
        //    ILogger log)
        //{
        //    //log.LogInformation("C# HTTP trigger function processed a request.");

        //    string name = req.Query["name"];

        //    string requestBody = await new StreamReader(req.Body).ReadToEndAsync();
        //    dynamic data = JsonConvert.DeserializeObject(requestBody);
        //    name = name ?? data?.name;

        //    string responseMessage = string.IsNullOrEmpty(name)
        //        ? "This HTTP triggered function executed successfully. Pass a name in the query string or in the request body for a personalized response."
        //        : $"Hello, {name}. This HTTP triggered function executed successfully.";

        //    return new OkObjectResult(responseMessage);
        //}
        //[FunctionName("Indexing")]
        //public static IActionResult Run(
        //    [HttpTrigger(AuthorizationLevel.Function, "get", "post", Route = null)] HttpRequest req,
        //    ILogger log)
        //{
        //    var conString = Environment.GetEnvironmentVariable("ConnectionString");
        //    //int minSequence = 1;
        //    //int maxSequence = Convert.ToInt32(Environment.GetEnvironmentVariable("PageSize"));
        //    var con = new SqlConnection(conString);

        //    con.Open();
        //    IList<Content> contents = null;
        //    IDbCommand com = new SqlCommand();
        //    com.Connection = con;
        //    com.CommandText = "spGetDataToIndex";
        //    com.CommandType = CommandType.StoredProcedure;
        //    SqlDataReader reader = (SqlDataReader)com.ExecuteReader();
        //    if (reader.HasRows)
        //    {
        //        contents = new List<Content>();
        //        while (reader.Read())
        //        {
        //            Content content = new Content();
        //            content.ContentID = Convert.ToInt32(reader["ContentID"].ToString());
        //            content.Type = reader["Type"].ToString();
        //            content.Name = reader["Name"].ToString();
        //            content.Year = reader["Year"].ToString();
        //            content.Platform = reader["Platform"].ToString();
        //            content.Language = reader["Language"].ToString();
        //            content.Genre = reader["Genre"].ToString();
        //            content.Rating = reader["Rating"].ToString();
        //            content.Comment = reader["Comment"].ToString();
        //            content.Watchlink = reader["WatchLink"].ToString();
        //            content.ImageUrl = reader["ImageUrl"].ToString();
        //            content.Plot = reader["Plot"].ToString();
        //            content.Sequence = Convert.ToInt32(reader["Sequence"].ToString());
        //            contents.Add(content);
        //        }
        //    }
        //    reader.Close();
        //    //string responseMessage = contents.Count.ToString();

        //    DataTable ContentIDTable = new DataTable();
        //    ContentIDTable.Columns.Add("ContentID",typeof(Int32));
        //    DataRow row = ContentIDTable.NewRow();
        //    row["ContentID"] = 51;
        //    ContentIDTable.Rows.Add(row);
        //    com = new SqlCommand("spUpdateIndexValues", con);
        //    com.CommandType = CommandType.StoredProcedure;
        //    SqlParameter param = new SqlParameter("@ContentIDs", ContentIDTable);
        //    param.SqlDbType = SqlDbType.Structured;
        //    param.TypeName = "dbo.ContentIDTVP";
        //    com.Parameters.Add(param);
        //    int i = com.ExecuteNonQuery();
        //    return new OkObjectResult(i.ToString());
        //}
        //[FunctionName("test")]
        //public static IActionResult Runtest(
        //    [HttpTrigger(AuthorizationLevel.Function, "get", "post", Route = null)] HttpRequest req,
        //    ILogger log)
        //{
        //    var conString = Environment.GetEnvironmentVariable("ConnectionString");

        //    var con = new SqlConnection(conString);

        //    con.Open();
        //    IList<Content> contents = null;
        //    IDbCommand com = new SqlCommand();
        //    com.Connection = con;
        //    com.CommandText = "spGetContents";
        //    com.CommandType = CommandType.StoredProcedure;
        //    SqlParameter parameter = new SqlParameter("@minSequence", 1);
        //    com.Parameters.Add(parameter);
        //    parameter = new SqlParameter("@maxSequence", 50);
        //    com.Parameters.Add(parameter);
        //    //parameter = new SqlParameter("@maxSequence", SqlDbType.Int);
        //    //parameter.Direction = ParameterDirection.Output;
        //    SqlDataReader reader = (SqlDataReader)com.ExecuteReader();
        //    if (reader.HasRows)
        //    {
        //        contents = new List<Content>();
        //        while (reader.Read())
        //        {
        //            Content content = new Content();
        //            content.ContentID = Convert.ToInt32(reader["ContentID"].ToString());
        //            content.Type = reader["Type"].ToString();
        //            content.Name = reader["Name"].ToString();
        //            content.Year = reader["Year"].ToString();
        //            content.Platform = reader["Platform"].ToString();
        //            content.Language = reader["Language"].ToString();
        //            content.Genre = reader["Genre"].ToString();
        //            content.Rating = reader["Rating"].ToString();
        //            content.Comment = reader["Comment"].ToString();
        //            content.Watchlink = reader["WatchLink"].ToString();
        //            content.ImageUrl = reader["ImageUrl"].ToString();
        //            content.Plot = reader["Plot"].ToString();
        //            content.Sequence = Convert.ToInt32(reader["Sequence"].ToString());
        //            contents.Add(content);
        //        }
        //    }
        //    //return contents;


        //    string responseMessage = contents.Count.ToString();
        //    return new OkObjectResult(responseMessage);
        //}
    }
}
