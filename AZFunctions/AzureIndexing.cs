using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Microsoft.Azure.Search;
using Microsoft.Azure.Search.Models;
using System.Collections.Generic;
using Entities;
using System.Data.SqlClient;
using System.Data;
using Microsoft.Azure;
using Microsoft.Azure.Storage;
using System.Configuration;

namespace AZFunctions
{
    public static class AzureIndexing
    {
        //[FunctionName("AzureIndexing")]
        //public static IActionResult Run(
        //    [HttpTrigger(AuthorizationLevel.Function, "get", "post", Route = null)] HttpRequest req,
        //    ILogger log)
        //{
        //    //string azureSearchName = ConfigurationManager.AppSettings["AzureSearchName"].ToString();
        //    string responseMessage = string.Empty;
        //    IList<AzureIndexClass> aziClass = MapToAzureIndexClass(GetContents());
        //    if(aziClass == null || aziClass.Count == 0)
        //    {
        //        responseMessage = "no data to index";
        //        return new OkObjectResult(responseMessage);
        //    }
        //    responseMessage = DoIndexing(aziClass);
        //    if (responseMessage == "success")
        //    {
        //        responseMessage = UpdateIndexedItems(aziClass);
        //    }
        //    return new OkObjectResult(responseMessage);
        //}
        
        //public static IList<Content> GetContents()
        //{
        //    var conString = Environment.GetEnvironmentVariable("ConnectionString");
        //    var con = new SqlConnection(conString);
        //    con.Open();
        //    IList<Content> contents = null;
        //    IDbCommand com = new SqlCommand();
        //    com.Connection = con;
        //    com.CommandText = "spGetDataToIndex";
        //    com.CommandType = CommandType.StoredProcedure;
        //    SqlDataReader reader = (SqlDataReader)com.ExecuteReader();
        //    if (reader.HasRows)
        //    {
        //        contents = new List<Content>();
        //        while (reader.Read())
        //        {
        //            Content content = new Content();
        //            content.ContentID = Convert.ToInt32(reader["ContentID"].ToString());
        //            //content.ContentID = reader["ContentID"].ToString();
        //            content.StringContentID = reader["ContentID"].ToString();
        //            content.Type = reader["Type"].ToString();
        //            content.Name = reader["Name"].ToString();
        //            content.Year = reader["Year"].ToString();
        //            content.Platform = reader["Platform"].ToString();
        //            content.Language = reader["Language"].ToString();
        //            content.Genre = reader["Genre"].ToString();
        //            content.Rating = reader["Rating"].ToString();
        //            content.Comment = reader["Comment"].ToString();
        //            content.Watchlink = reader["WatchLink"].ToString();
        //            content.ImageUrl = reader["ImageUrl"].ToString();
        //            content.Plot = reader["Plot"].ToString();
        //            content.Sequence = Convert.ToInt32(reader["Sequence"].ToString());
        //            content.Tags = reader["tags"].ToString();
        //            contents.Add(content);
        //        }
        //    }
        //    reader.Close();
        //    return contents;
        //}
        //public static IList<AzureIndexClass> MapToAzureIndexClass(IList<Content> contents)
        //{
        //    IList<AzureIndexClass> lstAziClass = new List<AzureIndexClass>();
        //    AzureIndexClass aziClass = null;
        //    if(contents == null || contents.Count == 0)
        //    {
        //        return lstAziClass;
        //    }
        //    foreach(var item in contents)
        //    {
        //        aziClass = new AzureIndexClass();
        //        aziClass.StringContentID = item.ContentID.ToString();
        //        aziClass.Type = item.Type;
        //        aziClass.Name = item.Name;
        //        aziClass.Year = item.Year;
        //        aziClass.Platform = item.Platform;
        //        aziClass.Language = item.Language;
        //        aziClass.Genre = item.Genre;
        //        aziClass.Rating = item.Rating;
        //        aziClass.Comment = item.Comment;
        //        aziClass.Watchlink = item.Watchlink;
        //        aziClass.Plot = item.Plot;
        //        aziClass.ImageUrl = item.ImageUrl;
        //        aziClass.Tags = item.Tags;
        //        lstAziClass.Add(aziClass);
        //    }
        //    return lstAziClass;

        //}
        //public static string DoIndexing(IList<AzureIndexClass> contents)
        //{
            
        //    if (contents == null || contents.Count == 0)
        //    {
        //        return "no data to index";
        //    }
            
        //    var azureSearchName = "azuresearchforjubamo";
        //    var azureSearchKey = "FC2012375F143A8E695F273FD885F61E";
        //    string indexName = "jubamo-content";
        //    ISearchServiceClient azureSearchService = new SearchServiceClient(azureSearchName, new SearchCredentials(azureSearchKey));
        //    ISearchIndexClient indexClient = azureSearchService.Indexes.GetClient(indexName);
        //    //if (azureSearchService.Indexes.Exists(indexName))
        //    //{
        //    //    azureSearchService.Indexes.Delete(indexName);
        //    //}
        //    Index indexModel = new Index()
        //    {
        //        Name = indexName,
        //        Fields = new[]
        //        {
        //            new Field("StringContentID",DataType.String) { IsKey = true, IsRetrievable = true, IsFacetable = false },
        //            new Field("Type", DataType.String) {IsRetrievable = true, IsSearchable = false, IsFacetable = false },
        //            new Field("Name", DataType.String) {IsRetrievable = true, IsSearchable = false, IsFacetable = false },
        //            new Field("Year", DataType.String) {IsSearchable = false, IsRetrievable = true, IsFilterable = true, IsFacetable = false },
        //            new Field("Platform", DataType.String) { IsSearchable=false, IsFilterable = true, IsRetrievable = true, IsSortable = true, IsFacetable = false },
        //            new Field("Language", DataType.String) { IsSearchable=false, IsFacetable = true, IsFilterable= true, IsRetrievable = true },
        //            new Field("Genre", DataType.String) { IsSearchable=false, IsRetrievable = true, IsFacetable = false },
        //            new Field("Rating", DataType.String) {IsRetrievable = true, IsSearchable = false, IsFacetable = false },
        //            new Field("Comment", DataType.String) {IsSearchable = false, IsRetrievable = true, IsFilterable = true, IsFacetable = false },
        //            new Field("Watchlink", DataType.String) { IsSearchable=false, IsFilterable = true, IsRetrievable = true, IsSortable = true, IsFacetable = false },
        //            new Field("Plot", DataType.String) { IsSearchable=false, IsFilterable = true, IsRetrievable = true, IsSortable = true, IsFacetable = false },
        //            new Field("ImageUrl", DataType.String) { IsSearchable=false, IsFilterable = true, IsRetrievable = true, IsSortable = true, IsFacetable = false },
        //            new Field("Tags", DataType.String) { IsSearchable = true,IsFilterable = true, IsRetrievable = true, IsSortable = true, IsFacetable = false },

        //        }
        //    };

                        
        //    // Create the Index in AzureSearch
        //    var resultIndex = azureSearchService.Indexes.CreateOrUpdate(indexModel);
        //    //var listBooks = new BookModel().GetBooks();
        //    indexClient.Documents.Index(IndexBatch.MergeOrUpload<AzureIndexClass>(contents));
        //    return "success";
        //}
        //private static void Search(ISearchIndexClient indexClient, string searchText, string filter = null, List<string> order = null, List<string> facets = null)
        //{
        //    var sp = new SearchParameters();
        //    // Add Filter
        //    if (!String.IsNullOrEmpty(filter))
        //    {
        //        sp.Filter = filter;
        //    }

        //    // Order
        //    if (order != null && order.Count > 0)
        //    {
        //        sp.OrderBy = order;
        //    }

        //    // facets
        //    if (facets != null && facets.Count > 0)
        //    {
        //        sp.Facets = facets;
        //    }

        //    DocumentSearchResult<AzureIndexClass> response = indexClient.Documents.Search<AzureIndexClass>(searchText, sp);
        //    foreach (SearchResult<AzureIndexClass> result in response.Results)
        //    {
        //        //Console.WriteLine(result.Document + " - Score: " + result.Score);
        //    }
        //    if (response.Facets != null)
        //    {
        //        foreach (var facet in response.Facets)
        //        {
        //            Console.WriteLine("\n Facet Name: " + facet.Key);
        //            foreach (var value in facet.Value)
        //            {
        //                Console.WriteLine("Value :" + value.Value + " - Count: " + value.Count);
        //            }
        //        }
        //    }
        //}
        //public static string UpdateIndexedItems(IList<AzureIndexClass> contents)
        //{
        //    var conString = Environment.GetEnvironmentVariable("ConnectionString");
        //    var con = new SqlConnection(conString);

        //    con.Open();
        //    IDbCommand com = new SqlCommand();
        //    com.Connection = con;
        //    DataTable ContentIDTable = new DataTable();
        //    ContentIDTable.Columns.Add("ContentID", typeof(Int32));
        //    DataRow row = null;
        //    try
        //    {
        //        foreach (var item in contents)
        //        {
        //            row = ContentIDTable.NewRow();
        //            row["ContentID"] = Convert.ToInt32(item.StringContentID);
        //            ContentIDTable.Rows.Add(row);
        //        }

        //        com = new SqlCommand("spUpdateIndexValues", con);
        //        com.CommandType = CommandType.StoredProcedure;
        //        SqlParameter param = new SqlParameter("@ContentIDs", ContentIDTable);
        //        param.SqlDbType = SqlDbType.Structured;
        //        param.TypeName = "dbo.ContentIDTVP";
        //        com.Parameters.Add(param);
        //        com.ExecuteNonQuery();
        //        return "Data Updated Successfully";
        //    }
        //    catch (Exception ex)
        //    {
        //        return "Some error occured in data updation " + ex.Message + "stack trace " + ex.StackTrace
        //            + "Inner Exception " + ex.InnerException;
        //    }

        //}

    }
}
