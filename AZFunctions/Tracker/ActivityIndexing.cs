using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System.Data.SqlClient;
using System.Data;
using System.Collections.Generic;
using Microsoft.Azure.Search;
using Microsoft.Azure.Search.Models;
using Entities.Tracker;

namespace AZFunctions.Tracker
{
    public static class ActivityIndexing
    {
        [FunctionName("ActivityIndexing")]
        public static IActionResult Run(
            [HttpTrigger(AuthorizationLevel.Function, "get", "post", Route = null)] HttpRequest req,
            ILogger log)
        {
            string responseMessage = "testing";
            //IList<ActivityIndexModel> documentIndexModels = Map(GetActivites());
            //if (documentIndexModels == null || documentIndexModels.Count == 0)
            //{
            //    responseMessage = "no data to index";
            //    return new OkObjectResult(responseMessage);
            //}
            //responseMessage = DoIndexing(documentIndexModels);
            //if (responseMessage == "success")
            //{
            //    responseMessage = UpdateIndexedItems(documentIndexModels);
            //}
            return new OkObjectResult(responseMessage);
        }
        //public static IList<Activity> GetActivites()
        //{
        //    var conString = Environment.GetEnvironmentVariable("ConnectionString");
        //    var con = new SqlConnection(conString);
        //    con.Open();
        //    IList<Activity> activites = null;
        //    IDbCommand com = new SqlCommand();
        //    com.Connection = con;
        //    com.CommandText = "spGetActivitiesToIndex";
        //    com.CommandType = CommandType.StoredProcedure;
        //    SqlDataReader reader = (SqlDataReader)com.ExecuteReader();
        //    if (reader.HasRows)
        //    {
        //        activites = new List<Activity>();
        //        while (reader.Read())
        //        {
        //            Activity activity = new Activity();
        //            activity.ActivityID = Convert.ToInt32(reader["ActivityID"].ToString());
        //            activity.Name = reader["Name"].ToString();
        //            activity.Detail = reader["Detail"].ToString();
        //            activity.CreatedOn = Convert.ToDateTime(reader["CreatedOn"].ToString());
        //            activity.Tags = reader["Tags"].ToString();
        //            activity.DocUrl = reader["DocUrl"].ToString();
        //            activites.Add(activity);
        //        }
        //    }
        //    reader.Close();
        //    return activites;
        //}
        //public static IList<ActivityIndexModel> Map(IList<Activity> activities)
        //{
        //    IList<ActivityIndexModel> activityIndexModels = new List<ActivityIndexModel>();
        //    ActivityIndexModel activityIndexModel = null;
        //    if (activities == null || activities.Count == 0)
        //    {
        //        return activityIndexModels;
        //    }
        //    foreach (var item in activities)
        //    {
        //        activityIndexModel = new ActivityIndexModel();
        //        activityIndexModel.StringActivityID = item.ActivityID.ToString();
        //        activityIndexModel.Name = item.Name;
        //        activityIndexModel.Detail = item.Detail;
        //        activityIndexModel.Tags = item.Tags;
        //        activityIndexModel.CreatedOn = item.CreatedOn;
        //        activityIndexModel.DocUrl = item.DocUrl;
        //        activityIndexModels.Add(activityIndexModel);
        //    }
        //    return activityIndexModels;

        //}
        //public static string DoIndexing(IList<ActivityIndexModel> activityIndexModels)
        //{

        //    if (activityIndexModels == null || activityIndexModels.Count == 0)
        //    {
        //        return "no data to index";
        //    }

        //    var azureSearchName = "azuresearchforjubamo";
        //    var azureSearchKey = "FC2012375F143A8E695F273FD885F61E";
        //    string indexName = "jubamo-tracker";
        //    ISearchServiceClient azureSearchService = new SearchServiceClient(azureSearchName, new SearchCredentials(azureSearchKey));
        //    ISearchIndexClient indexClient = azureSearchService.Indexes.GetClient(indexName);
        //    //if (azureSearchService.Indexes.Exists(indexName))
        //    //{
        //    //    azureSearchService.Indexes.Delete(indexName);
        //    //}
        //    Index indexModel = new Index()
        //    {
        //        Name = indexName,
        //        Fields = new[]
        //        {
        //            new Field("StringActivityID",DataType.String) { IsKey = true, IsRetrievable = true, IsFacetable = false },
        //            new Field("Name", DataType.String) {IsRetrievable = true, IsSearchable = true, IsFacetable = false },
        //            new Field("Detail", DataType.String) {IsRetrievable = true, IsSearchable = true, IsFacetable = false },
        //            new Field("Tags", DataType.String) { IsSearchable=true, IsFilterable = true, IsRetrievable = true, IsSortable = true, IsFacetable = false },
        //            new Field("CreatedOn", DataType.DateTimeOffset) {IsSearchable = false, IsRetrievable = true, IsFilterable = true, IsFacetable = false },
        //            new Field("DocUrl", DataType.String) {IsSearchable = false, IsRetrievable = true, IsFilterable = false, IsFacetable = false },
        //        }
        //    };


        //    // Create the Index in AzureSearch
        //    var resultIndex = azureSearchService.Indexes.CreateOrUpdate(indexModel);
        //    //var listBooks = new BookModel().GetBooks();
        //    indexClient.Documents.Index(IndexBatch.MergeOrUpload<ActivityIndexModel>(activityIndexModels));
        //    return "success";
        //}
        //public static string UpdateIndexedItems(IList<ActivityIndexModel> activityIndexModels)
        //{
        //    var conString = Environment.GetEnvironmentVariable("ConnectionString");
        //    var con = new SqlConnection(conString);

        //    con.Open();
        //    IDbCommand com = new SqlCommand();
        //    com.Connection = con;
        //    DataTable ContentIDTable = new DataTable();
        //    ContentIDTable.Columns.Add("ContentID", typeof(Int32));
        //    DataRow row = null;
        //    try
        //    {
        //        foreach (var item in activityIndexModels)
        //        {
        //            row = ContentIDTable.NewRow();
        //            row["ContentID"] = Convert.ToInt32(item.StringActivityID);
        //            ContentIDTable.Rows.Add(row);
        //        }

        //        com = new SqlCommand("spUpdateActivityIndexValues", con);
        //        com.CommandType = CommandType.StoredProcedure;
        //        SqlParameter param = new SqlParameter("@ContentIDs", ContentIDTable);
        //        param.SqlDbType = SqlDbType.Structured;
        //        param.TypeName = "dbo.ContentIDTVP";
        //        com.Parameters.Add(param);
        //        com.ExecuteNonQuery();
        //        return "Data Updated Successfully";
        //    }
        //    catch (Exception ex)
        //    {
        //        return "Some error occured in data updation " + ex.Message + "stack trace " + ex.StackTrace
        //            + "Inner Exception " + ex.InnerException;
        //    }

        //}
    }
}
