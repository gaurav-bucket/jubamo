﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AZFunctions.Tracker
{
    public class ActivityIndexModel
    {
        public string StringActivityID
        {
            get; set;
        }
       
        public string Name
        {
            get; set;
        }
        public string Detail
        {
            get; set;
        }
        public string Tags
        {
            get; set;
        }
        public DateTime CreatedOn
        {
            get; set;
        }
        public string DocUrl
        {
            get;set;
        }
    }
}
