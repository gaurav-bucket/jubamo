using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System.Data.SqlClient;
using System.Data;
using System.Collections.Generic;
using Microsoft.Azure.Search;
using Microsoft.Azure.Search.Models;

namespace AZFunctions.Document
{
    public static class DocumentIndexing
    {
        [FunctionName("DocumentsIndexing")]
        public static IActionResult Run(
            [HttpTrigger(AuthorizationLevel.Function, "get", "post", Route = null)] HttpRequest req,
            ILogger log)
        {
            string responseMessage = string.Empty;
            IList<DocumentIndexModel> documentIndexModels = Map(GetDocuments());
            if (documentIndexModels == null || documentIndexModels.Count == 0)
            {
                responseMessage = "no data to index";
                return new OkObjectResult(responseMessage);
            }
            responseMessage = DoIndexing(documentIndexModels);
            if (responseMessage == "success")
            {
                responseMessage = UpdateIndexedItems(documentIndexModels);
            }
            return new OkObjectResult(responseMessage);
        }
        public static IList<Entities.Document> GetDocuments()
        {
            var conString = Environment.GetEnvironmentVariable("ConnectionString");
            var con = new SqlConnection(conString);
            con.Open();
            IList<Entities.Document> documents = null;
            IDbCommand com = new SqlCommand();
            com.Connection = con;
            com.CommandText = "spGetDocumentDataToIndex";
            com.CommandType = CommandType.StoredProcedure;
            SqlDataReader reader = (SqlDataReader)com.ExecuteReader();
            if (reader.HasRows)
            {
                documents = new List<Entities.Document>();
                while (reader.Read())
                {
                    Entities.Document document = new Entities.Document();
                    document.DocumentID = Convert.ToInt32(reader["DocumentID"].ToString());
                    document.stringDocumentID = reader["DocumentID"].ToString();
                    document.Type = reader["Type"].ToString();
                    document.Name = reader["Name"].ToString();
                    document.DocUrl = reader["DocUrl"].ToString();
                    document.Tags = reader["Tags"].ToString();
                    documents.Add(document);
                }
            }
            reader.Close();
            return documents;
        }
        public static IList<DocumentIndexModel> Map(IList<Entities.Document> documents)
        {
            IList<DocumentIndexModel> diModels = new List<DocumentIndexModel>();
            DocumentIndexModel diModel = null;
            if (documents == null || documents.Count == 0)
            {
                return diModels;
            }
            foreach (var item in documents)
            {
                diModel = new DocumentIndexModel();
                diModel.StringDocumentID = item.stringDocumentID;
                diModel.Type = item.Type;
                diModel.Name = item.Name;
                diModel.DocUrl = item.DocUrl;
                diModel.Tags = item.Tags;
                diModels.Add(diModel);
            }
            return diModels;

        }
        public static string DoIndexing(IList<DocumentIndexModel> documentIndexModels)
        {

            if (documentIndexModels == null || documentIndexModels.Count == 0)
            {
                return "no data to index";
            }

            var azureSearchName = "azuresearchforjubamo";
            var azureSearchKey = "FC2012375F143A8E695F273FD885F61E";
            string indexName = "jubamo-document";
            ISearchServiceClient azureSearchService = new SearchServiceClient(azureSearchName, new SearchCredentials(azureSearchKey));
            ISearchIndexClient indexClient = azureSearchService.Indexes.GetClient(indexName);
            //if (azureSearchService.Indexes.Exists(indexName))
            //{
            //    azureSearchService.Indexes.Delete(indexName);
            //}
            Index indexModel = new Index()
            {
                Name = indexName,
                Fields = new[]
                {
                    new Field("StringDocumentID",DataType.String) { IsKey = true, IsRetrievable = true, IsFacetable = false },
                    new Field("Type", DataType.String) {IsRetrievable = true, IsSearchable = false, IsFacetable = false },
                    new Field("Name", DataType.String) {IsRetrievable = true, IsSearchable = false, IsFacetable = false },
                    new Field("DocUrl", DataType.String) {IsSearchable = false, IsRetrievable = true, IsFilterable = true, IsFacetable = false },
                    new Field("Tags", DataType.String) { IsSearchable=true, IsFilterable = true, IsRetrievable = true, IsSortable = true, IsFacetable = false }
                }
            };


            // Create the Index in AzureSearch
            var resultIndex = azureSearchService.Indexes.CreateOrUpdate(indexModel);
            //var listBooks = new BookModel().GetBooks();
            indexClient.Documents.Index(IndexBatch.MergeOrUpload<DocumentIndexModel>(documentIndexModels));
            return "success";
        }
        public static string UpdateIndexedItems(IList<DocumentIndexModel> documentIndexModels)
        {
            var conString = Environment.GetEnvironmentVariable("ConnectionString");
            var con = new SqlConnection(conString);

            con.Open();
            IDbCommand com = new SqlCommand();
            com.Connection = con;
            DataTable ContentIDTable = new DataTable();
            ContentIDTable.Columns.Add("ContentID", typeof(Int32));
            DataRow row = null;
            try
            {
                foreach (var item in documentIndexModels)
                {
                    row = ContentIDTable.NewRow();
                    row["ContentID"] = Convert.ToInt32(item.StringDocumentID);
                    ContentIDTable.Rows.Add(row);
                }

                com = new SqlCommand("spUpdateDocumentIndexValues", con);
                com.CommandType = CommandType.StoredProcedure;
                SqlParameter param = new SqlParameter("@ContentIDs", ContentIDTable);
                param.SqlDbType = SqlDbType.Structured;
                param.TypeName = "dbo.ContentIDTVP";
                com.Parameters.Add(param);
                com.ExecuteNonQuery();
                return "Data Updated Successfully";
            }
            catch (Exception ex)
            {
                return "Some error occured in data updation " + ex.Message + "stack trace " + ex.StackTrace
                    + "Inner Exception " + ex.InnerException;
            }

        }
    }
}
