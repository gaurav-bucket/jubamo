﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AZFunctions.Document
{
    public class DocumentIndexModel
    {
        public string StringDocumentID
        {
            get; set;
        }
        public string Type
        {
            get; set;
        }
        public string Name
        {
            get; set;
        }
        public string DocUrl
        {
            get; set;
        }
        public string Tags
        {
            get; set;
        }
    }
}
