//using System;
//using System.IO;
//using System.Threading.Tasks;
//using Microsoft.AspNetCore.Mvc;
//using Microsoft.Azure.WebJobs;
//using Microsoft.Azure.WebJobs.Extensions.Http;
//using Microsoft.AspNetCore.Http;
//using Microsoft.Extensions.Logging;
//using Newtonsoft.Json;
//using System.Collections.Generic;
//using DAL;
//using Entities;
//using System.Data.SqlClient;
//using System.Data;
//using Lucene.Net.Util;
//using Lucene.Net.Store;
//using Lucene.Net.Analysis.Standard;
//using Lucene.Net.Index;
//using Lucene.Net.Documents;
//using Lucene.Net.Store.Azure;
//using Microsoft.Azure.Storage;
//using Microsoft.Azure.Storage.Blob;
//using System.Configuration;

//namespace AZFunctions
//{
//    public static class DataIndexing
//    {
//        [FunctionName("DataIndexing")]
//        public static IActionResult Run(
//            [HttpTrigger(AuthorizationLevel.Function, "get", "post", Route = null)] HttpRequest req,
//            ILogger log)
//        {
//            IList<Content> contents = GetContents();
//            string responseMessage = DoIndexing(contents);
//            if(responseMessage == "Successfully Indexed")
//            {
//                responseMessage = UpdateIndexedItems(contents);
//            }
//            return new OkObjectResult(responseMessage);
//        }
//        public static IList<Content> GetContents()
//        {
//            var conString = Environment.GetEnvironmentVariable("ConnectionString");
//            var con = new SqlConnection(conString);
//            con.Open();
//            IList<Content> contents = null;
//            IDbCommand com = new SqlCommand();
//            com.Connection = con;
//            com.CommandText = "spGetDataToIndex";
//            com.CommandType = CommandType.StoredProcedure;
//            SqlDataReader reader = (SqlDataReader)com.ExecuteReader();
//            if (reader.HasRows)
//            {
//                contents = new List<Content>();
//                while (reader.Read())
//                {
//                    Content content = new Content();
//                    content.ContentID = Convert.ToInt32(reader["ContentID"].ToString());
//                    content.Type = reader["Type"].ToString();
//                    content.Name = reader["Name"].ToString();
//                    content.Year = reader["Year"].ToString();
//                    content.Platform = reader["Platform"].ToString();
//                    content.Language = reader["Language"].ToString();
//                    content.Genre = reader["Genre"].ToString();
//                    content.Rating = reader["Rating"].ToString();
//                    content.Comment = reader["Comment"].ToString();
//                    content.Watchlink = reader["WatchLink"].ToString();
//                    content.ImageUrl = reader["ImageUrl"].ToString();
//                    content.Plot = reader["Plot"].ToString();
//                    content.Sequence = Convert.ToInt32(reader["Sequence"].ToString());
//                    content.Tags = reader["tags"].ToString();
//                    contents.Add(content);
//                }
//            }
//            reader.Close();
//            return contents;
//        }
//        public static string DoIndexingOld(IList<Content> contents)
//        {
//            var AppLuceneVersion = LuceneVersion.LUCENE_48;
//            var indexLocation = @"http://www.jubamo.com/index";
//            var dir = FSDirectory.Open(indexLocation);

//            //create an analyzer to process the text
//            var analyzer = new StandardAnalyzer(AppLuceneVersion);

//            //create an index writer
//            var indexConfig = new IndexWriterConfig(AppLuceneVersion, analyzer);
//            var writer = new IndexWriter(dir, indexConfig);
//            Document document = null;
//            try
//            {
//                foreach (var item in contents)
//                {
//                    document = new Document
//                {
//                    new Int32Field("ContentID",item.ContentID,Field.Store.YES),
//                    new StringField("Name",item.Name,Field.Store.YES),
//                    new StringField("Year",item.Year,Field.Store.YES),
//                    new StringField("Platform",item.Platform,Field.Store.YES),
//                    new StringField("Language",item.Language,Field.Store.YES),
//                    new StringField("Genre",item.Genre,Field.Store.YES),
//                    new StringField("Rating",item.Rating,Field.Store.YES),
//                    new StringField("Comment",item.Comment,Field.Store.YES),
//                    new StringField("Watchlink",item.Watchlink,Field.Store.YES),
//                    new StringField("Name",item.ImageUrl,Field.Store.YES),
//                    new Int32Field("Sequence",item.Sequence,Field.Store.YES),
//                    new StringField("Plot",item.Plot,Field.Store.YES),
//                    new TextField("Tags",item.Tags,Field.Store.YES)
//                };
//                    writer.AddDocument(document);
//                }

//                writer.Commit();
//                writer.Dispose();
//                return "Successfully Indexed";
//            }
//            catch(Exception ex)
//            {
//                return "Somme error occurred " + ex.Message;
//            }
//        }
//        public static string DoIndexing(IList<Content> contents)
//        {
//            var AppLuceneVersion = LuceneVersion.LUCENE_48;
//            //var config = ConfigurationManager.GetSection("AzureBlob");
            //CloudStorageAccount storageaccount = CloudStorageAccount.Parse("DefaultEndpointsProtocol=https;AccountName=thestaticcontent;AccountKey=PlDVzwBK+O527tN00W86ldDlU2Jyzw0UmEhj2r3NIeBBHF3MYGhM87ccYyPa5n22r8VbBJUMt080rJPco11FKA==;EndpointSuffix=core.windows.net");
//            //private static AzureDirectory azureDirectory = new AzureDirectory(CloudStorageAccount.Parse(CloudConfigurationManager.GetSetting("StorageConnectionString")), "SearchCatalog");
//        //CloudStorageAccount storageaccount = CloudStorageAccount.Parse(Environment.GetEnvironmentVariable("BlobStorageAccount"));

//        //CloudBlobClient client = storageaccount.CreateCloudBlobClient();
//        //Get the reference of the Container. The GetConainerReference doesn't make a request to the Blob Storage but the Create() & CreateIfNotExists() method does. The method CreateIfNotExists() could be use whether the Container exists or not  
//        //CloudBlobContainer container = client.GetContainerReference("lucene");
//        //container.SetPermissionsAsync(new BlobContainerPermissions { PublicAccess = BlobContainerPublicAccessType.Container });
//        var cacheDirectory = new RAMDirectory();

//            var indexName = "lucene";
//            //CloudBlobDirectory directory = container.GetDirectoryReference("indexes");
//            var azureDirectory = new AzureDirectory(storageaccount, indexName, cacheDirectory);

//            //create an analyzer to process the text
//            var analyzer = new StandardAnalyzer(AppLuceneVersion);

//            //create an index writer
//            var indexConfig = new IndexWriterConfig(AppLuceneVersion, analyzer);
//            var writer = new IndexWriter(azureDirectory, indexConfig);
//            Document document = null;
//            try
//            {
//                foreach (var item in contents)
//                {
//                    document = new Document
//                {
//                    new Int32Field("ContentID",item.ContentID,Field.Store.YES),
//                    new StringField("Name",item.Name,Field.Store.YES),
//                    new StringField("Year",item.Year,Field.Store.YES),
//                    new StringField("Platform",item.Platform,Field.Store.YES),
//                    new StringField("Language",item.Language,Field.Store.YES),
//                    new StringField("Genre",item.Genre,Field.Store.YES),
//                    new StringField("Rating",item.Rating,Field.Store.YES),
//                    new StringField("Comment",item.Comment,Field.Store.YES),
//                    new StringField("Watchlink",item.Watchlink,Field.Store.YES),
//                    new StringField("Name",item.ImageUrl,Field.Store.YES),
//                    new Int32Field("Sequence",item.Sequence,Field.Store.YES),
//                    new StringField("Plot",item.Plot,Field.Store.YES),
//                    new TextField("Tags",item.Tags,Field.Store.YES)
//                };
//                    writer.AddDocument(document);
//                }

//                writer.Commit();
//                writer.Dispose();
//                return "Successfully Indexed";
//            }
//            catch (Exception ex)
//            {
//                return "Somme error occurred " + ex.Message + "stack trace " + ex.StackTrace
//                    + "Inner Exception " + ex.InnerException; ;
//            }
//        }
//        public static string UpdateIndexedItems(IList<Content> contents)
//        {
//            var conString = Environment.GetEnvironmentVariable("ConnectionString");
//            var con = new SqlConnection(conString);

//            con.Open();
//            IDbCommand com = new SqlCommand();
//            com.Connection = con;
//            DataTable ContentIDTable = new DataTable();
//            ContentIDTable.Columns.Add("ContentID", typeof(Int32));
//            DataRow row = null;
//            try
//            {
//                foreach (var item in contents)
//                {
//                    row = ContentIDTable.NewRow();
//                    row["ContentID"] = item.ContentID;
//                    ContentIDTable.Rows.Add(row);
//                }

//                com = new SqlCommand("spUpdateIndexValues", con);
//                com.CommandType = CommandType.StoredProcedure;
//                SqlParameter param = new SqlParameter("@ContentIDs", ContentIDTable);
//                param.SqlDbType = SqlDbType.Structured;
//                param.TypeName = "dbo.ContentIDTVP";
//                com.Parameters.Add(param);
//                com.ExecuteNonQuery();
//                return "Data Updated Successfully";
//            }
//            catch(Exception ex)
//            {
//                return "Some error occured in data updation " + ex.Message + "stack trace " + ex.StackTrace
//                    +"Inner Exception " + ex.InnerException;
//            }
            
//        }

//    }
//}
