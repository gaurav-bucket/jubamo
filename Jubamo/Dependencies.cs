﻿using DAL;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Jubamo
{
    public class Dependencies
    {
        public static void Resolve(IServiceCollection services, IConfiguration configuration)
        {
            services.AddTransient<IDbConnectionFactory>(x => new SqlConnectionFactory(configuration["AppKeys:AzureDbConnectionString"]));
            services.AddTransient<IContentRepository, ContentRepository>(); 
        }
    }
}
