var searchCriteria = {};
var searchQuery = '*';
var platform = '';
var genre = '';
var type = '';
var language = '';
var sectionType = 'IsAvailableSection';
var isChanged = "false";

function openNav() {
    document.getElementById("page-sidebar").style.width = "250px";
    document.getElementById("main").style.marginLeft = "250px";
}
    
function closeNav() {
    document.getElementById("page-sidebar").style.width = "0";
    document.getElementById("main").style.marginLeft= "0";
}
function readCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) === ' ') c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) === 0) return c.substring(nameEQ.length, c.length);
    }
    return null;
}

$(document).ready(function() {
    
    $('[data-toggle="popover"]').popover();

    
    $('.search-filter-list > li > a').on("click", function () {
        
        $('.search-filter-sublist').hide();
        $(this).next().fadeIn();
    });


    $('.search-filter-sublist a').on("click", function () {
        
        $('.search-filter-sublist a').removeClass("active");
        $(this).addClass("active");

    });

    var searchFilterWidth = $('#search-filter').outerWidth();
    $('#search-filter').css({right : -searchFilterWidth + "px"})
    $('#search-filter .filter-icon').on("click", function(){
        $("#search-filter").toggleClass("opened");
        if($("#search-filter").hasClass("opened")){
            $('#search-filter').animate({right : "0"})
        }
        else{
            $('#search-filter').animate({right : -searchFilterWidth + "px"})
        }
        
    });

    $(window).resize(function(){
        searchFilterWidth = $('#search-filter').outerWidth();
        $("#search-filter").removeClass("opened");
        $('#search-filter').animate({right : -searchFilterWidth + "px"})
    });

    $("#movie-featured .owl-carousel").owlCarousel({
        autoplay:true,
        autoplayHoverPause:true,
        autoplaySpeed: 4000,
        nav:true,
        center: false,
        loop:true,
        autoWidth:true,
        responsiveClass:true,
        responsive:{
            1200:{
                items:5
            }
        }
    });


    $("#ForgotPasswordLink").on("click", function(){
        $('.toggle-login, .toggle-register').hide();
        $('.toggle-forgot-password').show();
    });

    $("#RegisterLink").on("click", function(){
        $('.toggle-login, .toggle-forgot-password').hide();
        $('.toggle-register').show();
    });

    $(".user-comments-link").click(function(){
        $(this).parents(".movie-details").find(".user-comments").slideToggle();

    });

    $('#loader').show();
    setTimeout(function(){
        $('#loader').fadeOut();
    },1000);
    
    var availableTags = [
        "Movie1",
        "Movie2",
        "Movie3",
        "Movie4"
      ];
      //$( "#MoviewAutoComplete" ).autocomplete({
      //    source: contentNames
      //});
    $('#availableTab').on("click", function () {
        sectionType = "IsAvailableSection";
        //$.ajax({
        //    type: 'POST',
        //    url: '/search',
        //    data: { sectionType: sectionType, SearchCriteria: searchCriteria },
        //    success: function (result) {
        //        $("#movie-featured").hide();
        //        $('#availableSection').html(result);
        //        //$('#load-more-loader').hide();
        //        $('.load-more > span').hide();
        //    }
        //});

    });
    $('#recentlyReleasedTab').on("click", function () {
        //alert("hi");
        sectionType = "IsRecentlyReleasedSection";
        //$.ajax({
        //    type: 'POST',
        //    url: '/search',
        //    data: { sectionType: sectionType, SearchCriteria: searchCriteria },
        //    success: function (result) {
        //        $("#movie-featured").hide();
        //        $('#recentlyReleasedSection').html(result);
        //        //$('#load-more-loader').hide();
        //        $('.load-more > span').hide();
        //    }
        //});
    });

    $('.load-more > span').on("click", function () {
        trackLoadMore();
        prepareSearchQuery();
        $(this).hide();
        //$("#tabs-loader").hide();
          $('#load-more-loader').show();
          $.ajax({
              type: 'POST',
              url: '/loadmore',
              data: { sectionType: sectionType, searchQuery: searchQuery },
              success: function (result) {
                  $('#availableSection').append(result);
                  $('#load-more-loader').hide();
                  $('.load-more > span').show();
                  $('[data-toggle="popover"]').popover();
              }
          });
        //setTimeout(function(){
        //    $('#load-more-loader').hide();
        //    $('.load-more > span').show();
        //},1000);

    });
    
    $('#spnSearchMovie').on("click", function () {
        //alert($("#MoviewAutoComplete").val());
        trackSearchMovie($("#MoviewAutoComplete").val());
        $("#availableTab").text("Searched*");
        searchQuery = $("#MoviewAutoComplete").val();
        $.ajax({
            type: 'POST',
            url: '/search',
            data: {
                sectionType: 'IsAvailableSection', searchQuery: searchQuery, searchMode:"all" },
            success: function (result) {
                $("#movie-featured").hide();
                $('#availableSection').html(result);
                //$('#load-more-loader').hide();
                $('.load-more > span').hide();
                $('[data-toggle="popover"]').popover();
            }
        });
        
        

        //$.ajax({
        //    type: 'POST',
        //    url: '/search',
        //    data: {
        //        sectionType: 'IsRecentlyReleasedSection', searchQuery: searchQuery, searchMode: "all" },
        //    success: function (result) {
        //        $("#movie-featured").hide();
        //        $('#recentlyReleasedSection').html(result);
        //        //$('#load-more-loader').hide();
        //        $('.load-more > span').hide();
        //    }
        //});

    });
    $('#MoviewAutoComplete').keypress(function (e) {

        if (e.which === 13) {
            $('#spnSearchMovie').click();
            $("#selectedfilters").html('');
        }

    });
    $('#search-filter-clear').on("click", function () {
        $("#selectedfilters").html('');
        $('#MoviewAutoComplete').val('');
        $("#availableTab").text("All");
        searchQuery = '';
        platform = '';
        genre = '';
        type = '';
        langauge = '';
        searchQuery = "*";
        $.ajax({
            type: 'POST',
            url: '/search',
            data: {
                sectionType: 'IsAvailableSection', searchQuery: searchQuery },
            success: function (result) {
                $("#movie-featured").hide();
                $('#availableSection').html(result);
                //$('#load-more-loader').hide();
                //$('.load-more > span').hide();
                $('[data-toggle="popover"]').popover();
            }
        });
        //$.ajax({
        //    type: 'POST',
        //    url: '/search',
        //    data: {
        //        sectionType: 'IsRecentlyReleasedSection', searchQuery: searchQuery },
        //    success: function (result) {
        //        $("#movie-featured").hide();
        //        $('#recentlyReleasedSection').html(result);
        //        //$('#load-more-loader').hide();
        //        $('.load-more > span').hide();
        //    }
        //});

    });
    
    $('#platformOptions > li > a').on("click", function () {
        trackFilterClicked("Platform",$(this).text().trim());
        $("#availableTab").text("Filtered*");
        
        $('#MoviewAutoComplete').val('');
        searchQuery = '';
        
        $("#selectedfilters").append("<li>" + $(this).text().trim() + "</li >");
        if (platform !== '') {
            platform = platform + "|" + $(this).text().trim();
        }
        else {
            platform = $(this).text().trim();
        }
        prepareSearchQuery();
        $.ajax({
            type: 'POST',
            url: '/search',
            data: {
                sectionType: 'IsAvailableSection', searchQuery: searchQuery },
            success: function (result) {
                $("#movie-featured").hide();
                $('#availableSection').html(result);
                $('[data-toggle="popover"]').popover();
                $('.load-more > span').show();
            }
        });
        //$.ajax({
        //    type: 'POST',
        //    url: '/search',
        //    data: {
        //        sectionType: 'IsRecentlyReleasedSection', searchQuery: searchQuery },
        //    success: function (result) {
        //        $("#movie-featured").hide();
        //        $('#recentlyReleasedSection').html(result);
        //    }
        //});
    });

    $('#typeOptions > li > a').on("click", function () {
        trackFilterClicked("Type",$(this).text().trim());
        $("#availableTab").text("Filtered*");
        
        $('#MoviewAutoComplete').val('');
        searchQuery = '';
        
        $("#selectedfilters").append("<li>" + $(this).text().trim() + "</li >");
        if (type !== '') {
            type = type + "|" + $(this).text().trim();
        }
        else {
            type = $(this).text().trim();
        }
        prepareSearchQuery();
        $.ajax({
            type: 'POST',
            url: '/search',
            data: {
                sectionType: 'IsAvailableSection', searchQuery: searchQuery
            },
            success: function (result) {
                $("#movie-featured").hide();
                $('#availableSection').html(result);
                $('[data-toggle="popover"]').popover();
                $('.load-more > span').show();
            }
        });
        //$.ajax({
        //    type: 'POST',
        //    url: '/search',
        //    data: {
        //        sectionType: 'IsRecentlyReleasedSection', searchQuery: searchQuery
        //    },
        //    success: function (result) {
        //        $("#movie-featured").hide();
        //        $('#recentlyReleasedSection').html(result);
        //    }
        //});
    });

    $('#languageOptions > li > a').on("click", function () {
        trackFilterClicked("Language",$(this).text().trim());
        $("#availableTab").text("Filtered*");
        
        $('#MoviewAutoComplete').val('');
        searchQuery = '';
        
        $("#selectedfilters").append("<li>" + $(this).text().trim() + "</li >");
        if (language !== '') {
            language = language + "|" + $(this).text().trim();
        }
        else {
            language = $(this).text().trim();
        }
        prepareSearchQuery();
        $.ajax({
            type: 'POST',
            url: '/search',
            data: {
                sectionType: 'IsAvailableSection', searchQuery: searchQuery
            },
            success: function (result) {
                $("#movie-featured").hide();
                $('#availableSection').html(result);
                $('[data-toggle="popover"]').popover();
                $('.load-more > span').show();
            }
        });
        //$.ajax({
        //    type: 'POST',
        //    url: '/search',
        //    data: {
        //        sectionType: 'IsRecentlyReleasedSection', searchQuery: searchQuery
        //    },
        //    success: function (result) {
        //        $("#movie-featured").hide();
        //        $('#recentlyReleasedSection').html(result);
        //    }
        //});
    });


    
    $('#genreOptions > li > a').on("click", function () {
        trackFilterClicked("Genre",$(this).text().trim());
        $("#availableTab").text("Filtered*");
        
        $('#MoviewAutoComplete').val('');
        searchQuery = '';
        

        $("#selectedfilters").append("<li>" + $(this).text().trim() + "</li >");
        if (genre !== '') {
            genre = genre + "|" + $(this).text().trim();
        }
        else {
            genre = $(this).text().trim();
        }
        prepareSearchQuery();
        $.ajax({
            type: 'POST',
            url: '/search',
            data: {
                sectionType: 'IsAvailableSection', searchQuery: searchQuery },
            success: function (result) {
                $("#movie-featured").hide();
                $('#availableSection').html(result);
                //$('#load-more-loader').hide();
                //$('.load-more > span').hide();
                $('[data-toggle="popover"]').popover();
                $('.load-more > span').show();
            }
        });
        //$.ajax({
        //    type: 'POST',
        //    url: '/search',
        //    data: {
        //        sectionType: 'IsRecentlyReleasedSection', searchQuery: searchQuery
        //    },
        //    success: function (result) {
        //        $("#movie-featured").hide();
        //        $('#recentlyReleasedSection').html(result);
        //        //$('#load-more-loader').hide();
        //        //$('.load-more > span').hide();
        //    }
        //});
    });
    $('#imdbRatingOptions > li > a').on("click", function () {
        trackFilterClicked("ImdbRating", $(this).text().trim());
        $("#availableTab").text("Filtered*");

        $('#MoviewAutoComplete').val('');
        searchQuery = '';
        

        $("#selectedfilters").append("<li>" + $(this).text().trim() + "</li >");
        //if (imdbRating !== '') {
        //    imdbRating = imdbRating + "|" + $(this).text().trim();
        //}
        //else {
        //    imdbRating = $(this).text().trim();
        //}
        imdbRating = $(this).attr("id");
        prepareSearchQuery();
        $.ajax({
            type: 'POST',
            url: '/search',
            data: {
                sectionType: 'IsAvailableSection', searchQuery: searchQuery, imdbRating: imdbRating
            },
            success: function (result) {
                $("#movie-featured").hide();
                $('#availableSection').html(result);
                //$('#load-more-loader').hide();
                //$('.load-more > span').hide();
                $('[data-toggle="popover"]').popover();
                $('.load-more > span').show();
            }
        });
    });
    function prepareSearchQuery() {
        if (platform !== '' || genre !== '' || type !== '' || language !== '') {
            searchQuery = "(" + platform + ")" + "+" + "(" + genre + ")" + "+" + "(" + type + ")" + "+" + "(" + language + ")";
        }
        //searchCriteria.SearchQuery = searchQuery;
    }
      $('.nav-tabs a').on("click", function(){
        $('.load-more').hide();
        $("#tabs-loader").show();
        $('.tab-pane.active').hide();
        setTimeout(function(){
            $("#tabs-loader").hide();
            $('.tab-pane.active').show();
            $('.load-more').show();

        },1000);

      });

 });
 
