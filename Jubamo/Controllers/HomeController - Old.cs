﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Jubamo_Final.Models;
using Microsoft.Azure.Search;
using Entities;
using Newtonsoft.Json;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Entities.Jubamo;
using static Jubamo_Final.Models.Enums;

namespace Jubamo_Final.Controllers
{
    public class HomeController_Old : Controller
    {
        IConfiguration configuration;
        int loadCount_topSection = 0;
        int loadCount = 0;
        string azureSearchName = string.Empty;
        string azureSearchKey = string.Empty;
        string indexName = string.Empty;
        public HomeController_Old(IConfiguration configuration)
        {
            this.configuration = configuration;
        }
        private void InitializeConfigSettings()
        {
            loadCount_topSection = Convert.ToInt32(configuration["loadCount_topSection"]);
            loadCount = Convert.ToInt32(configuration["loadCount"]);
            azureSearchName = configuration["AzureSearchName"];
            azureSearchKey = configuration["AzureSearchKey"];
            indexName = configuration["indexName"];
        }
        public async Task<IActionResult> Index()
        {
            InitializeConfigSettings();
            IList<Content> contents = await GetSection(SectionType.TopSection.ToString(), loadCount_topSection, 0);
            ContentVM contentVM = new ContentVM();
            contentVM.TopSection = contents;
            contents = await GetSection(SectionType.AvailableSection.ToString(), loadCount, 0);
            contentVM.AvailableSection = contents;
            //contents = await GetSection(SectionType.RecentlyReleasedSection.ToString(), loadCount, 0);
            //contentVM.RecentlyReleasedSection = contents;
            return View(contentVM);
        }
        [Route("/loadmore")]
        [HttpPost]
        public async Task<PartialViewResult> LoadMore(string sectionType,SearchCriteria searchCriteria)
        {
            InitializeConfigSettings();
            int skip = Convert.ToInt32(HttpContext.Request.Cookies["skip"].ToString()) + loadCount;
            IList<Content> contents = await GetSection(sectionType, loadCount, skip);
            ContentVM contentVM = new ContentVM();
            contentVM.AvailableSection = contents;
            
            return new PartialViewResult
            {
                ViewName = sectionType,
                ViewData = new ViewDataDictionary<ContentVM>(ViewData, contentVM),
                
            };
        }
        //[Route("/search")]
        //[HttpPost]
        public async Task<PartialViewResult> Search(string sectionType, SearchCriteria searchCriteria)
        {
            InitializeConfigSettings();
            searchCriteria.Top = loadCount;
            if (searchCriteria.SearchQuery == null || searchCriteria.SearchQuery == string.Empty)
            {
                searchCriteria.SearchQuery = "*";
            }
            else
            {
                searchCriteria.SearchQuery = searchCriteria.SearchQuery.ToLower();
            }
            IList<Content> contents = await GetSection(sectionType, loadCount,0);
            //IList<Content> contents = await GetContentsAsync(searchCriteria);
            var contentsAvailable = contents;// contents.Where(s => s.Section != "2").ToList();
            ContentVM contentVM = new ContentVM();
            contentVM.AvailableSection = contentsAvailable;
            return new PartialViewResult
            {
                ViewName = sectionType,
                ViewData = new ViewDataDictionary<ContentVM>(ViewData, contentVM)
            };
        }
        private async Task<IList<Content>> GetSection(string type,int top, int skip)
        {
            SearchCriteria searchCriteria = new SearchCriteria();
            string searchQuery = string.Empty;
            //string indexName = string.Empty;
            IList<string> searchFields = null;
            string searchField = string.Empty;
            switch (type)
            {
                case "TopSection":
                    searchField = "IsTopSection";
                    break;
                case "AvailableSection":
                    searchField = "IsAvailableSection";
                    break;
                case "RecentlyReleasedSection":
                    searchField = "IsRecentlyReleasedSection";
                    break;
                default:
                    break;
            }
            searchQuery = "1";
            searchFields = new List<string>();
            searchFields.Add(searchField);
            searchCriteria.SearchQuery = searchQuery;
            searchCriteria.SearchFields = searchFields;
            IList<string> lstOrderBy = new List<string>();
            string orderBy = "Sequence desc";
            lstOrderBy.Add(orderBy);
            searchCriteria.OrderBy = lstOrderBy;
            //string filter = string.Empty;
            //IList<string> facets = null;
            //searchCriteria.Facets = facets;
            searchCriteria.Top = top;
            searchCriteria.Skip = skip;
            IList<Content> contents = await GetContentsAsync(searchCriteria);
            return contents;
        }
        private async Task<IList<Content>> GetContentsAsync(SearchCriteria searchCriteria)
        {
            ISearchServiceClient azureSearchService = new SearchServiceClient(azureSearchName, new SearchCredentials(azureSearchKey));
            ISearchIndexClient indexClient = azureSearchService.Indexes.GetClient(indexName);
            AZSearch azSearch = new AZSearch();
            IList<Content> contents = await azSearch.SearchAsync(indexClient,searchCriteria);
            SetSkip(searchCriteria.Skip.ToString());
            return contents;
        }
        private void SetSkip(string skip)
        {
            Microsoft.AspNetCore.Http.CookieOptions option = new Microsoft.AspNetCore.Http.CookieOptions();
            option.Expires = DateTime.Now.AddDays(10);
            option.IsEssential = true;
            Response.Cookies.Append("skip", skip, option);
        }
        public IActionResult Watchlist()
        {
            return View();
        }
        public IActionResult About()
        {
            return View();
        }
    }
}
