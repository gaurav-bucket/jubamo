﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Jubamo.Models;
using Microsoft.Azure.Search;
using Entities;
using Newtonsoft.Json;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Entities.Jubamo;
using static Jubamo.Models.Enums;

namespace Jubamo.Controllers
{
    public class HomeController : Controller
    {
        IConfiguration configuration;
        Common common;
        public HomeController(IConfiguration configuration)
        {
            this.configuration = configuration;
            common = new Common();
        }
        public async Task<IActionResult> Index()
        {
            SetUniqueUserID();
            common.InitializeConfigSettings(configuration);
            ContentVM contentVM = new ContentVM();
            contentVM.TopSection = await GetTopSectionData();
            contentVM.AvailableSection = await GetAvailableSectionData();
            SetSkip(common.skip.ToString(),Enums.SectionType.IsAvailableSection.ToString());
            //contentVM.RecentlyReleasedSection = await GetRecentlyReleasedSectionData();
            contentVM.StaticContentPath = common.staticContentPath;
            contentVM.MixpanelKey = common.mixpanelKey;
            contentVM.Environment = common.environment;
            //SetSkip(common.skip.ToString(), Enums.SectionType.IsRecentlyReleasedSection.ToString());
            return View(contentVM);
        }
        private void SetUniqueUserID()
        {
            if (Request.Cookies["userID"] != null)
            {
                return;
            }
            string userID = string.Empty;
            Guid g;
            g = Guid.NewGuid();
            Microsoft.AspNetCore.Http.CookieOptions option = new Microsoft.AspNetCore.Http.CookieOptions();
            option.Expires = DateTime.Now.AddDays(10);
            option.IsEssential = true;
            Response.Cookies.Append("userID", g.ToString(), option);

        }
        private async Task<IList<Content>> GetTopSectionData()
        {
            SearchCriteria searchCriteria = common.PrepareSearchCriteria(Enums.SectionType.IsTopSection.ToString(), common.pageSize_topSection,searchQuery: "*",skip: 0,searchMode: "any");
            return await common.GetContents(searchCriteria);
        }
        private async Task<IList<Content>> GetAvailableSectionData()
        {
            SearchCriteria searchCriteria = common.PrepareSearchCriteria(Enums.SectionType.IsAvailableSection.ToString(), common.pageSize, searchQuery: "*", skip: 0, searchMode: "any");
            return await common.GetContents(searchCriteria);
        }
        private async Task<IList<Content>> GetRecentlyReleasedSectionData()
        {
            SearchCriteria searchCriteria = common.PrepareSearchCriteria(Enums.SectionType.IsRecentlyReleasedSection.ToString(), common.pageSize, searchQuery: "*", skip: 0, searchMode: "any");
            return await common.GetContents(searchCriteria);
        }
        [Route("/search")]
        [HttpPost]
        public async Task<PartialViewResult> GetDataBySearch(string sectionType, string searchQuery,string searchMode="any", string imdbRating = "1")
        {
            common.InitializeConfigSettings(configuration);
            SearchCriteria searchCriteria = common.PrepareSearchCriteria(sectionType, common.pageSize, searchQuery,0,searchMode,imdbRating);
            ContentVM contentVM = new ContentVM();
            contentVM.Contents = await common.GetContents(searchCriteria);
            contentVM.StaticContentPath = common.staticContentPath;
            return new PartialViewResult
            {
                ViewName = "Contents",
                ViewData = new ViewDataDictionary<ContentVM>(ViewData, contentVM)
            };
        }
        

        [Route("/loadmore")]
        [HttpPost]
        public async Task<PartialViewResult> LoadMore(string sectionType, string searchQuery)
        {
            common.InitializeConfigSettings(configuration);
            int skip = 0;
            if (sectionType == Enums.SectionType.IsAvailableSection.ToString())
            {
                skip = Convert.ToInt32(HttpContext.Request.Cookies["skip_availableSection"].ToString()) + common.pageSize;

            }
            else if (sectionType == Enums.SectionType.IsRecentlyReleasedSection.ToString())
            {
                skip = Convert.ToInt32(HttpContext.Request.Cookies["skip_recentlyReleasedSection"].ToString()) + common.pageSize;
            }
            SearchCriteria searchCriteria = common.PrepareSearchCriteria(sectionType, common.pageSize, searchQuery, skip,searchMode:"any");
            IList<Content> contents = await common.GetContents(searchCriteria);
            ContentVM contentVM = new ContentVM();
            contentVM.Contents = contents;
            contentVM.StaticContentPath = common.staticContentPath;
            SetSkip(skip.ToString(), sectionType);
            return new PartialViewResult
            {
                ViewName = "LoadMoreSection",
                ViewData = new ViewDataDictionary<ContentVM>(ViewData, contentVM),

            };
        }
        private void SetSkip(string skip,string sectionType)
        {
            Microsoft.AspNetCore.Http.CookieOptions option = new Microsoft.AspNetCore.Http.CookieOptions();
            option.Expires = DateTime.Now.AddDays(10);
            option.IsEssential = true;
            if(sectionType == Enums.SectionType.IsAvailableSection.ToString())
            {
                Response.Cookies.Append("skip_availableSection", skip, option);
            }
            else if (sectionType == Enums.SectionType.IsRecentlyReleasedSection.ToString())
            {
                Response.Cookies.Append("skip_recentlyReleasedSection", skip, option);
            }
        }
        public IActionResult Watchlist()
        {
            return View();
        }
        public IActionResult About()
        {
            return View();
        }
    }
}
