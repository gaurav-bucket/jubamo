﻿using Entities;
using Entities.Jubamo;
using Microsoft.Azure.Search;
using Microsoft.Azure.Search.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Jubamo.Models
{
    public class AZSearch
    {
        public IList<Content> Search(ISearchIndexClient indexClient, string searchText, string filter = null, List<string> order = null, List<string> facets = null)
        {
            var sp = new SearchParameters();
            // Add Filter
            if (!String.IsNullOrEmpty(filter))
            {
                sp.Filter = filter;
            }

            // Order
            if (order != null && order.Count > 0)
            {
                sp.OrderBy = order;
            }

            // facets
            if (facets != null && facets.Count > 0)
            {
                sp.Facets = facets;
            }

            DocumentSearchResult<Content> response = indexClient.Documents.Search<Content>(searchText, sp);
            IList<Content> contents = new List<Content>();
            Content content = null;
            foreach (SearchResult<Content> result in response.Results)
            {
                content = new Content();
                content.ContentID = Convert.ToInt32(result.Document.StringContentID);
                content.Type = result.Document.Type;
                content.Name = result.Document.Name;
                content.Platform = result.Document.Platform;
                content.Year = result.Document.Year;
                content.Language = result.Document.Language;
                content.Genre = result.Document.Genre;
                content.Rating = result.Document.Rating;
                content.Watchlink = result.Document.Watchlink;
                content.ImageName = result.Document.ImageName;
                content.Plot = result.Document.Plot;
                content.Tags = result.Document.Tags;
                contents.Add(content);
            }
            return contents;
            //if (response.Facets != null)
            //{
            //    foreach (var facet in response.Facets)
            //    {
            //        Console.WriteLine("\n Facet Name: " + facet.Key);
            //        foreach (var value in facet.Value)
            //        {
            //            Console.WriteLine("Value :" + value.Value + " - Count: " + value.Count);
            //        }
            //    }
            //}
        }
        public async Task<IList<Content>> SearchAsync(ISearchIndexClient indexClient, string searchText, string filter, IList<string> order, IList<string> facets,IList<string> searchFields,int top, int skip)
        {
            var sp = new SearchParameters();
            // Add Filter
            if (!String.IsNullOrEmpty(filter))
            {
                sp.Filter = filter;
            }

            // Order
            if (order != null && order.Count > 0)
            {
                sp.OrderBy = order;
            }

            // facets
            if (facets != null && facets.Count > 0)
            {
                sp.Facets = facets;
            }

            // search fields
            if (searchFields != null && searchFields.Count > 0)
            {
                sp.SearchFields = searchFields;
            }

            sp.Top = top;
            sp.Skip = skip;

            DocumentSearchResult<Content> response = await indexClient.Documents.SearchAsync<Content>(searchText, sp);
            IList<Content> contents = new List<Content>();
            Content content = null;
            foreach (SearchResult<Content> result in response.Results)
            {
                content = new Content();
                content.ContentID = Convert.ToInt32(result.Document.StringContentID);
                content.Type = result.Document.Type;
                content.Name = result.Document.Name;
                content.Platform = result.Document.Platform;
                content.Year = result.Document.Year;
                content.Language = result.Document.Language;
                content.Genre = result.Document.Genre;
                content.Rating = result.Document.Rating;
                content.Watchlink = result.Document.Watchlink;
                content.ImageName = result.Document.ImageName;
                content.Plot = result.Document.Plot;
                content.Tags = result.Document.Tags;
                //content.Section = result.Document.Section;
                content.Sequence = result.Document.Sequence;
                contents.Add(content);
            }
            return contents;
            //if (response.Facets != null)
            //{
            //    foreach (var facet in response.Facets)
            //    {
            //        Console.WriteLine("\n Facet Name: " + facet.Key);
            //        foreach (var value in facet.Value)
            //        {
            //            Console.WriteLine("Value :" + value.Value + " - Count: " + value.Count);
            //        }
            //    }
            //}
        }
        public async Task<IList<Content>> SearchAsync(ISearchIndexClient indexClient, SearchCriteria searchCriteria)
        {
            var sp = new SearchParameters();
            switch (searchCriteria.SearchMode)
            {
                case "all":
                    sp.SearchMode = SearchMode.All;
                    break;
                default:
                    sp.SearchMode = SearchMode.Any;
                    break;
            }
            
            // Add Filter
            if (!String.IsNullOrEmpty(searchCriteria.Filter))
            {
                sp.Filter = searchCriteria.Filter;
            }

            // Order
            if (searchCriteria.OrderBy != null && searchCriteria.OrderBy.Count > 0)
            {
                sp.OrderBy = searchCriteria.OrderBy;
            }

            // facets
            if (searchCriteria.Facets != null && searchCriteria.Facets.Count > 0)
            {
                sp.Facets = searchCriteria.Facets;
            }

            // search fields
            if (searchCriteria.SearchFields != null && searchCriteria.SearchFields.Count > 0)
            {
                sp.SearchFields = searchCriteria.SearchFields;
            }

            sp.Top = searchCriteria.Top;
            sp.Skip = searchCriteria.Skip;

            DocumentSearchResult<Content> response = await indexClient.Documents.SearchAsync<Content>(searchCriteria.SearchQuery, sp);
            IList<Content> contents = new List<Content>();
            Content content = null;
            foreach (SearchResult<Content> result in response.Results)
            {
                content = new Content();
                content.ContentID = Convert.ToInt32(result.Document.StringContentID);
                content.Type = result.Document.Type;
                content.Name = result.Document.Name;
                content.Platform = result.Document.Platform;
                content.Year = result.Document.Year;
                content.Language = result.Document.Language;
                content.Genre = result.Document.Genre;
                content.Rating = result.Document.Rating;
                content.Watchlink = result.Document.Watchlink;
                content.ImageName = result.Document.ImageName;
                content.ImageSource = result.Document.ImageSource;
                content.Plot = result.Document.Plot;
                content.Tags = result.Document.Tags;
                //content.Section = result.Document.Section;
                content.Sequence = result.Document.Sequence;
                contents.Add(content);
            }
            return contents;
            //if (response.Facets != null)
            //{
            //    foreach (var facet in response.Facets)
            //    {
            //        Console.WriteLine("\n Facet Name: " + facet.Key);
            //        foreach (var value in facet.Value)
            //        {
            //            Console.WriteLine("Value :" + value.Value + " - Count: " + value.Count);
            //        }
            //    }
            //}
        }
    }
}
