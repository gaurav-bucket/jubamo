﻿using Entities;
using Entities.Jubamo;
using Microsoft.Azure.Search;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Jubamo.Models
{
    public class Common
    {
        public int pageSize_topSection = 0;
        public int pageSize = 0;
        public int skip = 0;
        string azureSearchName = string.Empty;
        string azureSearchKey = string.Empty;
        string indexName = string.Empty;
        public string staticContentPath = string.Empty;
        public string mixpanelKey = string.Empty;
        public string environment = string.Empty;
        public void InitializeConfigSettings(IConfiguration configuration)
        {
            pageSize_topSection = Convert.ToInt32(configuration["pageSize_topSection"]);
            pageSize = Convert.ToInt32(configuration["pageSize"]);
            azureSearchName = configuration["AzureSearchName"];
            azureSearchKey = configuration["AzureSearchKey"];
            indexName = configuration["indexName"];
            staticContentPath = configuration["staticContentPath"];
            mixpanelKey = configuration["mixpanel_key"];
            environment = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
        }
        public async Task<IList<Content>> GetContents(SearchCriteria searchCriteria)
        {
            IList<Content> contents = await GetContentsAsync(searchCriteria);
            return contents;
        }
        public SearchCriteria PrepareSearchCriteria(string sectionType,int pageSize,string searchQuery,int skip,string searchMode,string imdbRating = "1")
        {
            SearchCriteria searchCriteria = new SearchCriteria();
            //if (sectionType != "")
            //{
            //    IList<string> searchFields = new List<string>();
            //    searchFields.Add(sectionType);
            //    searchCriteria.SearchFields = searchFields;
            //}
            searchCriteria.Filter = sectionType + " eq" + " '1'" + " and ImdbRating gt " + float.Parse(imdbRating);
            IList<string> lstOrderBy = new List<string>();
            lstOrderBy.Add("Sequence desc");
            searchCriteria.SearchQuery = searchQuery;
            searchCriteria.OrderBy = lstOrderBy;
            searchCriteria.Top = pageSize;
            searchCriteria.Skip = skip;
            searchCriteria.SearchMode = searchMode;
            return searchCriteria;
        }
        
        private async Task<IList<Content>> GetContentsAsync(SearchCriteria searchCriteria)
        {
            ISearchServiceClient azureSearchService = new SearchServiceClient(azureSearchName, new SearchCredentials(azureSearchKey));
            ISearchIndexClient indexClient = azureSearchService.Indexes.GetClient(indexName);
            AZSearch azSearch = new AZSearch();
            IList<Content> contents = await azSearch.SearchAsync(indexClient, searchCriteria);
            return contents;
        }
        
    }
}
