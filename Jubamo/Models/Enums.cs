﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Jubamo.Models
{
    public class Enums
    {
        public enum SectionType
        {
            IsTopSection,
            IsAvailableSection,
            IsRecentlyReleasedSection
        }
    }
}
