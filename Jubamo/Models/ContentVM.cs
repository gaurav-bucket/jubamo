﻿using Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Jubamo.Models
{
    public class ContentVM
    {
        public string StaticContentPath
        {
            get;set;
        }
        public IList<Content> Contents
        {
            get;set;
        }
        public IList<Content> TopSection
        {
            get;set;
        }
        public IList<Content> AvailableSection
        {
            get; set;
        }
        public IList<Content> ComingSoonSection
        {
            get; set;
        }
        public IList<Content> RecentlyReleasedSection
        {
            get; set;
        }
        public string Environment
        {
            get;set;
        }
        public string MixpanelKey
        {
            get;set;
        }
    }
}
