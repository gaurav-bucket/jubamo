﻿using System;
using System.Collections.Generic;
using System.Text;
using Entities;
namespace DAL
{
    public interface IContentRepository
    {
        IList<Content> Get(int minSequence, int maxSequence);
    }
}
