﻿using System;
using System.Collections.Generic;
using System.Text;
using Entities.Timeline;
namespace DAL
{
    public interface IUserRepository
    {
        int AuthenticateOrSave(User user);
    }
}
