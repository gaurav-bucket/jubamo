﻿using System;
using System.Collections.Generic;
using System.Text;
using Entities;
using Entities.Tracker;
namespace DAL
{
    public interface IActivityRepository
    {
        IList<Activity> Get();
        int Save(Activity activity);
    }
}
