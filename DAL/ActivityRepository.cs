﻿using Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using Entities.Tracker;
namespace DAL
{
    public class ActivityRepository : IActivityRepository
    {
        private IDbConnection con;

        public ActivityRepository(IDbConnectionFactory dbConnectionFactory)
        {
            con = dbConnectionFactory.CreateConnection();
        }
        public IList<Activity> Get()
        {
            IList<Activity> activities = null;
            IDbCommand com = new SqlCommand();
            com.Connection = con;
            com.CommandText = "spGetActivities";
            com.CommandType = CommandType.StoredProcedure;
            SqlDataReader reader = (SqlDataReader)com.ExecuteReader();
            if (reader.HasRows)
            {
                activities = new List<Activity>();
                while (reader.Read())
                {
                    Activity activity = new Activity();
                    activity.ActivityID = Convert.ToInt32(reader["ActivityID"].ToString());
                    activity.Name = reader["Name"].ToString();
                    activity.Detail = reader["Detail"].ToString();
                    activity.Tags = reader["Tags"].ToString();
                    activity.CreatedOn = Convert.ToDateTime(reader["CreatedOn"].ToString());
                    activity.DocUrl = reader["DocUrl"].ToString();
                    activities.Add(activity);
                }
            }
            return activities;
        }
        public int Save(Activity activity)
        {
            int isIndexed = 0;
            IDbCommand com = new SqlCommand();
            com.Connection = con;
            com.CommandText = "spSaveActivity";
            com.CommandType = CommandType.StoredProcedure;
            SqlParameter parameter = new SqlParameter("@name", activity.Name);
            com.Parameters.Add(parameter);
            parameter = new SqlParameter("@detail", activity.Detail);
            com.Parameters.Add(parameter);
            parameter = new SqlParameter("@tags", activity.Tags);
            com.Parameters.Add(parameter);
            parameter = new SqlParameter("@createdon", activity.CreatedOn);
            com.Parameters.Add(parameter);
            parameter = new SqlParameter("@isindexed", isIndexed);
            com.Parameters.Add(parameter);
            parameter = new SqlParameter("@docurl", activity.DocUrl);
            com.Parameters.Add(parameter);
            int result = com.ExecuteNonQuery();
            if(result > 0)
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }
    }
}
