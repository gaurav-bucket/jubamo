﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using Entities.Timeline;

namespace DAL
{
    public class UserRepository : IUserRepository
    {
        private IDbConnection con;

        public UserRepository(IDbConnectionFactory dbConnectionFactory)
        {
            con = dbConnectionFactory.CreateConnection();
        }
        public int AuthenticateOrSave(User user)
        {
            IDbCommand com = new SqlCommand();
            com.Connection = con;
            com.CommandText = "spAuthenticateOrSave";
            com.CommandType = CommandType.StoredProcedure;
            SqlParameter parameter = new SqlParameter("@username", user.Name);
            com.Parameters.Add(parameter);
            parameter = new SqlParameter("@password", user.Password);
            com.Parameters.Add(parameter);
            SqlParameter outputparameter = new SqlParameter("@output", SqlDbType.Int);
            outputparameter.Direction = ParameterDirection.Output;
            com.Parameters.Add(outputparameter);
            com.ExecuteNonQuery();
            int result = Convert.ToInt32(outputparameter.Value);
            return result;
        }
    }
}
