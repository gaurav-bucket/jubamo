﻿using Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using Entities.Tracker;
namespace DAL
{
    public class SprintRepository : ISprintRepository
    {
        private IDbConnection con;

        public SprintRepository(IDbConnectionFactory dbConnectionFactory)
        {
            con = dbConnectionFactory.CreateConnection();
        }
        public IList<Sprint> Get()
        {
            IList<Sprint> sprints = null;
            IDbCommand com = new SqlCommand();
            com.Connection = con;
            com.CommandText = "spGetSprints";
            com.CommandType = CommandType.StoredProcedure;
            SqlDataReader reader = (SqlDataReader)com.ExecuteReader();
            if (reader.HasRows)
            {
                sprints = new List<Sprint>();
                while (reader.Read())
                {
                    Sprint sprint = new Sprint();
                    sprint.SprintID = Convert.ToInt32(reader["SprintID"].ToString());
                    sprint.Name = reader["Name"].ToString();
                    sprint.DateRange = reader["DateRange"].ToString();
                    sprint.StoryPoints_Committed = Convert.ToInt32(reader["StoryPoints_Committed"].ToString());
                    sprint.StoryPoints_Delivered = Convert.ToInt32(reader["StoryPoints_Delivered"].ToString());
                    sprint.Bugs_Raised = Convert.ToInt32(reader["Bugs"].ToString());
                    sprint.Patches_Count = Convert.ToInt32(reader["Patches"].ToString());
                    sprint.Comments = reader["Comments"].ToString();
                    sprint.ReleaseName = reader["ReleaseName"].ToString();
                    sprint.ReleaseDate = reader["ReleaseDate"].ToString();
                    sprints.Add(sprint);
                }
            }
            return sprints;
        }
    }
}
