﻿using Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace DAL
{
    public class ContentRepository : IContentRepository
    {
        private IDbConnection con;

        public ContentRepository(IDbConnectionFactory dbConnectionFactory)
        {
            con = dbConnectionFactory.CreateConnection();
        }
        public IList<Content> Get(int minSequence, int maxSequence)
        {
            IList<Content> contents = null;
            IDbCommand com = new SqlCommand();
            com.Connection = con;
            com.CommandText = "spGetContents";
            com.CommandType = CommandType.StoredProcedure;
            SqlParameter parameter = new SqlParameter("@minSequence", minSequence);
            com.Parameters.Add(parameter);
            parameter = new SqlParameter("@maxSequence", maxSequence);
            com.Parameters.Add(parameter);
            //parameter = new SqlParameter("@maxSequence", SqlDbType.Int);
            //parameter.Direction = ParameterDirection.Output;
            SqlDataReader reader = (SqlDataReader)com.ExecuteReader();
            if (reader.HasRows)
            {
                contents = new List<Content>();
                while (reader.Read())
                {
                    Content content = new Content();
                    content.ContentID = Convert.ToInt32(reader["ContentID"].ToString());
                    //content.ContentID = reader["ContentID"].ToString();
                    content.Type = reader["Type"].ToString();
                    content.Name = reader["Name"].ToString();
                    content.Year = reader["Year"].ToString();
                    content.Platform = reader["Platform"].ToString();
                    content.Language = reader["Language"].ToString();
                    content.Genre = reader["Genre"].ToString();
                    content.Rating = reader["Rating"].ToString();
                    content.ImdbRating = float.Parse(reader["ImdbRating"].ToString());
                    content.Comment = reader["Comment"].ToString();
                    content.Watchlink = reader["WatchLink"].ToString();
                    content.ImageName = reader["ImageName"].ToString();
                    content.ImageSource = reader["ImageSource"].ToString();
                    content.Plot = reader["Plot"].ToString();
                    content.Sequence = Convert.ToInt32(reader["Sequence"].ToString());
                    content.Tags = reader["Tags"].ToString();
                    contents.Add(content);
                }
            }
            return contents;
        }
    }
}
