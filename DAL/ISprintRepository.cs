﻿using System;
using System.Collections.Generic;
using System.Text;
using Entities;
using Entities.Tracker;
namespace DAL
{
    public interface ISprintRepository
    {
        IList<Sprint> Get();
    }
}
